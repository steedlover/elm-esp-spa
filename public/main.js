const LANGUAGE_CONFIG = {
        DEFAULT: "en"
    },
    SERVER_CONFIG = {
        PROTO: "http://",
        HOST: "localhost",
        PORT: "3000",
        API: "api"
    },
    BROWSER_STORAGE = {
        LANGUAGE: "language",
        USER: "user",
        TOKEN: "token",
        TOKEN_VALIDATION: "isTokenValid",
        LANGUAGES: "getLanguages"
    },
    baseElement = document.getElementById("app"),
    messageElement = document.createElement("div");

messageElement.innerText = "Loading...";
baseElement.appendChild(messageElement);

function buildUrl(config) {
    let result = "";
    try {
        result += config.PROTO + config.HOST + (config.PORT ? ":" + config.PORT : "");
    } catch (e) {
        console.warn(e);
    }
    return result;
}

function queryStringSlash(url, ...params) {
    return params.reduce((acc, el) => acc + "/" + el, url);
}

function getLanguagesURL() {
    return queryStringSlash(buildUrl(SERVER_CONFIG), SERVER_CONFIG.API, BROWSER_STORAGE.LANGUAGES);
}

function getTokenValidationURL() {
    return queryStringSlash(buildUrl(SERVER_CONFIG), SERVER_CONFIG.API, BROWSER_STORAGE.TOKEN_VALIDATION);
}

function httpGetAsync(theUrl, token) {
    return new Promise((resolve) => {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", theUrl, true);
        if (token && typeof token === "string") {
            xmlHttp.setRequestHeader("Authorization", "Bearer " + token);
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                resolve(xmlHttp.responseText);
            }
        };
        xmlHttp.send(null);
    });
}

async function checkToken(user, token) {
    //user = {name: "admin", email: "mo@mo.ru"};
    //token =
    //"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwidXNlckFnZW50IjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvODYuMC40MjQwLjE5OCBTYWZhcmkvNTM3LjM2IiwiaWF0IjoxNjA2NDI0OTYzLCJleHAiOjE2MDY0MjUwMjN9.gnwoNXE1hXcIj84eGTASBG3atCWZRwprnqeW1aVZoK4";
    if (!user || !token) {
        return {status: 205, message: "No credentials were given"};
    } else {
        return await httpGetAsync(getTokenValidationURL(), token)
            .then((response) => {
                return JSON.parse(response);
            })
            .catch((error) => {
                return JSON.parse(error);
            });
    }
}

function isJson(item) {
    item = typeof item !== "string" ? JSON.stringify(item) : item;

    try {
        item = JSON.parse(item);
    } catch (e) {
        return false;
    }

    if (typeof item === "object" && item !== null) {
        return true;
    }

    return false;
}

function removeLocalStorageUser() {
    window.localStorage.removeItem(BROWSER_STORAGE.USER);
    window.localStorage.removeItem(BROWSER_STORAGE.TOKEN);
}

// Get translations first
(async function getStartData() {
    try {
        const flags = {
            screenWidth: parseInt(window.innerWidth)
        };
        flags.defLang = window.localStorage.getItem(BROWSER_STORAGE.LANGUAGE) || LANGUAGE_CONFIG.DEFAULT;
        const user = window.localStorage.getItem(BROWSER_STORAGE.USER),
            token = window.localStorage.getItem(BROWSER_STORAGE.TOKEN);
        [languages, tokenValidation] = await Promise.all([httpGetAsync(getLanguagesURL()), checkToken(user, token)]);
        if (!isJson(languages)) {
            throw {status: 400, message: "Wrong server response"};
        }
        languages = JSON.parse(languages);
        if (languages.status !== 200) {
            throw languages;
        }
        flags.languages = languages.data.languages;
        //console.log(tokenValidation);
        if (tokenValidation.status === 403) {
            removeLocalStorageUser();
            flags.errorMessage = tokenValidation.message;
        } else if (tokenValidation.status > 205) {
            throw tokenValidation;
        } else if (tokenValidation.status === 200) {
            flags.user = JSON.parse(user);
            flags.token = token;
        }

        baseElement.removeChild(messageElement);
        //console.log(flags, "flags");
        executeMainApp(flags);
    } catch (error) {
        try {
            messageElement.innerText = error.message || error.response.data.message;
        } catch (e) {
            console.warn(error);
        }
    }
})();

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function executeMainApp(flags) {
    var app = Elm.Main.init({flags});

    window.onresize = debounce(function () {
        app.ports.windowResize.send(parseInt(window.innerWidth));
    }, 50);

    if (io) {
        const sockClient = io.connect(buildUrl(SERVER_CONFIG));
        sockClient.on("add", (newObj) => app.ports.registerMachine.send(newObj));
        sockClient.on("remove", (id) => app.ports.removeMachine.send(id));
        sockClient.on("update", (newObj) => app.ports.updateMachine.send(newObj));
    }

    app.ports.outgoing.subscribe(function ({tag, data}) {
        switch (tag) {
            case "saveUser":
                window.localStorage.setItem(BROWSER_STORAGE.USER, JSON.stringify(data.user));
                window.localStorage.setItem(BROWSER_STORAGE.TOKEN, data.token);
                break;
            case "removeUser":
                removeLocalStorageUser();
                break;
            case "saveDefaultLanguage":
                window.localStorage.setItem(BROWSER_STORAGE.LANGUAGE, data);
                break;
            case "addTextareaListener":
                setTimeout(function () {
                    var textarea = document.getElementById(data);
                    if (textarea) {
                        textarea.addEventListener(
                            "paste",
                            function (e) {
                                setTimeout(function () {
                                    app.ports.setNewTextareaText.send(e.target.value);
                                }, 0);
                                e.stopPropagation();
                            },
                            false
                        );
                    }
                }, 100);
                break;
        }
    });
}
