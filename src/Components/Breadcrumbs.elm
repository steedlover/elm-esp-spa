module Components.Breadcrumbs exposing (view)

import Components.Link as CustomLink
import Core.Api.Languages as Languages
import Core.Config.Layout as Layout
import Core.Utils.Route exposing (getRouteByPath)
import Core.Utils.String as ExtraString exposing (htmlSpecialCharsDecode)
import Element
import I18Next exposing (Translations)
import Spa.Generated.Route as Route exposing (Route)
import Spa.Url exposing (Url)


type alias Config params msg =
    { url : Url params
    , translation : Maybe Translations
    , onClick : msg
    }


view : Config params (Route -> msg) -> Element.Element msg
view options =
    Element.row
        [ Element.width Element.fill
        , Element.paddingXY 0 (Layout.boxMargin // 2)
        ]
        [ Element.wrappedRow
            ( List.concat
                [ List.singleton <| Element.width Element.fill
                , Layout.lightLayoutStylesList
                ]
            )
            (breadcrumbsList
                { translation = options.translation
                , url = options.url
                , onClick = options.onClick
                }
            )
        ]


breadcrumbsList : Config params (Route -> msg) -> List (Element.Element msg)
breadcrumbsList options =
    let
        pathArr =
            List.filter (\e -> String.length e > 0) <|
                String.split "/" options.url.rawUrl.path

        lastElIndex =
            List.length pathArr - 1

        separator =
            htmlSpecialCharsDecode "&nbsp;&rarr;&nbsp;"
    in
    List.concat
        [ List.singleton <|
            linkView
                { click = options.onClick
                , route =
                    Maybe.withDefault Route.NotFound <|
                        getRouteByPath "/"
                , label =
                    Languages.translateWithDefault options.translation "BREADCRUMBS.HOME" "/"
                }
        , if List.length pathArr > 0 then
            [ Element.column
                [ Element.height Element.fill ]
                [ Element.text separator ]
            ]

          else
            [ Element.none ]
        , List.indexedMap
            (\i e ->
                Element.row
                    []
                    [ if i < lastElIndex then
                        linkView
                            { click = options.onClick
                            , route =
                                Maybe.withDefault Route.NotFound <|
                                    getRouteByPath e
                            , label =
                                Languages.translateWithDefault
                                    options.translation
                                    ("BREADCRUMBS." ++ String.toUpper e)
                                    (ExtraString.capitalize e)
                            }

                      else
                        Element.text <|
                            Languages.translateWithDefault
                                options.translation
                                ("BREADCRUMBS." ++ String.toUpper e)
                                (ExtraString.capitalize e)
                    , if i < lastElIndex then
                        Element.text separator

                      else
                        Element.none
                    ]
            )
            pathArr
        ]


linkView :
    { click : Route -> msg
    , label : String
    , route : Route
    }
    -> Element.Element msg
linkView options =
    CustomLink.view
        { label = options.label
        , style = Just CustomLink.Black
        , size = Just CustomLink.Big
        , click = options.click options.route
        }
