module Components.Button exposing (Size(..), Style(..), view)

import Core.Config.Colors as Colors
import Element exposing (text)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html.Attributes


type alias ButtonColorSchema =
    { mainColor : Element.Color
    , borderColor : Element.Color
    , fontColor : Element.Color
    , hoverMainColor : Element.Color
    , hoverBorderColor : Element.Color
    , hoverFontColor : Element.Color
    }


primaryColorSchema : ButtonColorSchema
primaryColorSchema =
    { mainColor = Colors.primaryColor
    , borderColor = Colors.primaryColor
    , fontColor = Colors.white
    , hoverMainColor = Colors.primaryColorDark
    , hoverBorderColor = Colors.primaryColorDarker
    , hoverFontColor = Colors.white
    }


disabledColorSchema : ButtonColorSchema
disabledColorSchema =
    { mainColor = Colors.lightGrey
    , borderColor = Colors.lightGrey
    , fontColor = Colors.secondaryColorDark
    , hoverMainColor = Colors.lightGrey
    , hoverBorderColor = Colors.lightGrey
    , hoverFontColor = Colors.secondaryColorDark
    }


secondaryColorSchema : ButtonColorSchema
secondaryColorSchema =
    { mainColor = Colors.secondaryColor
    , borderColor = Colors.secondaryColor
    , fontColor = Colors.white
    , hoverMainColor = Colors.secondaryColorDark
    , hoverBorderColor = Colors.secondaryColorDarker
    , hoverFontColor = Colors.white
    }


infoColorSchema : ButtonColorSchema
infoColorSchema =
    { mainColor = Colors.infoColor
    , borderColor = Colors.infoColor
    , fontColor = Colors.white
    , hoverMainColor = Colors.infoColorDark
    , hoverBorderColor = Colors.infoColorDarker
    , hoverFontColor = Colors.white
    }


dangerColorSchema : ButtonColorSchema
dangerColorSchema =
    { mainColor = Colors.dangerColor
    , borderColor = Colors.dangerColor
    , fontColor = Colors.white
    , hoverMainColor = Colors.infoColorDark
    , hoverBorderColor = Colors.infoColorDarker
    , hoverFontColor = Colors.white
    }


type Style
    = Primary
    | Disabled
    | Secondary
    | Info
    | Danger


type Size
    = Small
    | Normal
    | Large



-- VIEW


buttonInitialStyle : List (Element.Attribute msg)
buttonInitialStyle =
    [ Border.width 1
    , Font.size 14
    ]


view :
    { onClick : msg
    , label : Maybe String
    , buttonType : Maybe Style
    , radius : Maybe Int
    , size : Maybe Size
    , shadow : Maybe Element.Color
    , paddingX : Maybe Int
    , paddingY : Maybe Int
    }
    -> Element.Element msg
view options =
    let
        setPadding : Maybe Int -> Maybe Int -> List (Element.Attribute msg)
        setPadding x y =
            [ Element.paddingXY (Maybe.withDefault 10 x) (Maybe.withDefault 20 y)
            ]

        getColorSchema : ButtonColorSchema -> List (Element.Attribute msg)
        getColorSchema schema =
            [ Background.color schema.mainColor
            , Font.color schema.fontColor
            , Border.color schema.borderColor
            , Element.focused
                [ Background.color schema.hoverMainColor
                , Font.color schema.hoverFontColor
                ]
            , Element.mouseOver
                [ Background.color schema.hoverMainColor
                , Font.color schema.hoverFontColor
                ]
            ]

        setFontSize : Maybe Size -> List (Element.Attribute msg)
        setFontSize size =
            case size of
                Just s ->
                    case s of
                        Small ->
                            []

                        Normal ->
                            [ Font.size 18 ]

                        Large ->
                            [ Font.size 22 ]

                Nothing ->
                    []

        setShadow : Maybe Element.Color -> List (Element.Attribute msg)
        setShadow option =
            case option of
                Just color ->
                    [ Border.shadow
                        { offset = ( 0, 0 )
                        , size = 0
                        , blur = 15
                        , color = color
                        }
                    ]

                Nothing ->
                    []

        setStyle : Maybe Style -> List (Element.Attribute msg)
        setStyle style =
            case style of
                Just s ->
                    case s of
                        Primary ->
                            getColorSchema primaryColorSchema

                        Disabled ->
                            List.append (getColorSchema disabledColorSchema) []
                                |> List.append
                                    (List.singleton <|
                                        Region.description "The button is disabled"
                                    )
                                |> List.append
                                    (List.singleton <|
                                        Element.htmlAttribute <|
                                            Html.Attributes.style "cursor" "not-allowed"
                                    )

                        Secondary ->
                            getColorSchema secondaryColorSchema

                        Info ->
                            getColorSchema infoColorSchema

                        Danger ->
                            getColorSchema dangerColorSchema

                Nothing ->
                    getColorSchema primaryColorSchema

        setRoundBorder : Maybe Int -> List (Element.Attribute msg)
        setRoundBorder val =
            [ Border.rounded (Maybe.withDefault 10 val) ]
    in
    Input.button
        (List.append (setPadding options.paddingX options.paddingY) []
            |> List.append (setRoundBorder options.radius)
            |> List.append (setFontSize options.size)
            |> List.append (setStyle options.buttonType)
            |> List.append (setShadow options.shadow)
            |> List.append buttonInitialStyle
        )
        { onPress = Just options.onClick
        , label =
            Element.text <|
                case options.label of
                    Just l ->
                        l

                    Nothing ->
                        ""
        }
