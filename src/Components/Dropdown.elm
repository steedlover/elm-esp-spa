module Components.Dropdown exposing (Align(..), AllDropdowns(..), Config, State(..), view)

import Core.Config.Colors as Colors
import Core.Config.Layout exposing (noPadding)
import Element
    exposing
        ( alignLeft
        , alignRight
        , below
        , centerY
        , el
        , height
        , htmlAttribute
        , image
        , none
        , onRight
        , paddingEach
        , pointer
        , px
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Event
import Element.Font as Font
import Element.Input exposing (button)
import Html.Attributes exposing (style)


type AllDropdowns
    = AllClosed
    | UserMenuOpened
    | LanguageMenuOpened


type State
    = Ok Bool
    | Error


type Align
    = Left
    | Right


type alias Config msg =
    { fallback : Element.Element msg
    , rootOption : Maybe (Element.Element msg)
    , options : Maybe (List (Element.Element msg))
    , align : Maybe Align
    , toggle : msg
    }



---- VARIABLES


boxParams : List (Element.Attribute msg)
boxParams =
    [ paddingEach { top = 15, right = 10, bottom = 15, left = 10 }
    , Background.color Colors.darkGrey
    , Font.color Colors.whiteCream
    , Border.color Colors.lightGrey
    , Border.width 1
    , width <| px 250
    ]



---- VIEW


view :
    { state : State
    , config : Config msg
    }
    -> Element.Element msg
view options =
    let
        subOptions =
            case options.config.options of
                Just list ->
                    case options.state of
                        Ok state ->
                            if state == False then
                                []

                            else
                                List.singleton <|
                                    addElemsBelow
                                        (List.length list)
                                        list
                                        options.config.align

                        Error ->
                            []

                Nothing ->
                    []

        rootOption =
            case options.config.rootOption of
                Just root ->
                    case options.state of
                        Ok _ ->
                            root

                        Error ->
                            options.config.fallback

                Nothing ->
                    options.config.fallback
    in
    el
        (List.concat
            [ [ centerY
              , Font.size 18
              ]
            , case options.config.align of
                Just align ->
                    case align of
                        Left ->
                            List.singleton alignLeft

                        Right ->
                            [ alignRight
                            , paddingEach { noPadding | right = 40 }
                            ]

                Nothing ->
                    List.singleton alignLeft
            , subOptions
            ]
        )
        (el
            (case options.state of
                Ok state ->
                    List.singleton <|
                        onRight <|
                            renderCaret options.config.toggle state

                Error ->
                    []
            )
            (el
                [ pointer
                , Event.onClick <| options.config.toggle
                ]
                rootOption
            )
        )


renderCaret : a -> Bool -> Element.Element a
renderCaret msg state =
    let
        imgDesciption =
            if state == False then
                "Open"

            else
                "Close"

        caretRotation =
            if state == False then
                "rotate(0)"

            else
                "rotate(-90deg)"
    in
    button
        [ centerY
        , paddingEach { noPadding | left = 10 }
        ]
        { onPress = Just msg
        , label =
            image
                [ width <| px 15
                , height <| px 15
                , htmlAttribute <| style "transform" caretRotation
                ]
                { src = "/images/caret-down.png"
                , description = imgDesciption ++ " dropdown"
                }
        }


addElemsBelow : Int -> List (Element.Element msg) -> Maybe Align -> Element.Attribute msg
addElemsBelow lengthOrigin elems align =
    case elems of
        [] ->
            below none

        x :: xs ->
            below <|
                el
                    (List.concat <|
                        [ List.singleton (addElemsBelow lengthOrigin xs Nothing)
                        , [ htmlAttribute <| style "margin-left" "-1px"
                          ]
                        , case align of
                            Just side ->
                                case side of
                                    Right ->
                                        List.singleton <|
                                            htmlAttribute <|
                                                style "transform" "translateX(-110px)"

                                    Left ->
                                        []

                            Nothing ->
                                []
                        , getBoxParams (List.length elems) lengthOrigin
                        ]
                    )
                    x


getBoxParams : Int -> Int -> List (Element.Attribute msg)
getBoxParams length lengthOrigin =
    --Add margin to the very top option of the list
    if length == lengthOrigin then
        (htmlAttribute <| style "margin-top" "10px") :: boxParams

    else
        boxParams
