module Components.Editor exposing
    ( EditorModel
    , Validator(..)
    , create
    , trimJson
    , update
    , updateAndRerender
    , validatorToStringWithDefault
    , view
    )

import Array
import Components.Button as Button
import Core.Config.Colors as Colors
import Core.Config.Layout exposing (machineAttributeRow, noPadding)
import Core.Utils.Json as Json
import Css
import Dict
import Element
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Keyed as Keyed
import Html.Attributes
import Html.Styled as StyledHtml
import Html.Styled.Attributes as StyledAttributes
import Html.Styled.Events as StyledEvents
import Json.Decode as Decode
import Regex



-- TYPES


type Validator value
    = NoCheck
    | Valid value
    | Invalid value Decode.Error


type alias Config =
    { editorOn : Bool
    , id : String
    , sourceText : String
    , saveButtonLabel : String
    }


type alias EditorModel =
    { config : Config
    , source : Validator String
    , indent : String
    , key : Int
    , current : Validator String
    }


type alias ViewParams msg =
    { active : Bool
    , onChange : Bool -> String -> msg
    , onSubmit : msg
    }



-- VARIABLES


textAreaPadding : Int
textAreaPadding =
    10


textAreaLineHeight : Int
textAreaLineHeight =
    22



-- FUNCTIONS


create : Config -> EditorModel
create config =
    let
        indent =
            String.repeat 2 " "
    in
    { config = config
    , source = validate config.sourceText indent
    , indent = indent
    , key = 0
    , current = NoCheck
    }


update : EditorModel -> String -> EditorModel
update model newText =
    { model | current = validate newText model.indent }


updateAndRerender : EditorModel -> String -> EditorModel
updateAndRerender model newText =
    { model | current = validate newText model.indent, key = model.key + 1 }


validate : String -> String -> Validator String
validate json indent =
    case Decode.decodeString Json.decoder json of
        Ok data ->
            Valid <| jsonObjectView data 0 indent

        Err err ->
            Invalid json err


trimJson : String -> String
trimJson json =
    let
        linesArr : List String
        linesArr =
            String.lines json

        column : String
        column =
            ":"

        addColumn : String -> String -> String
        addColumn s1 s2 =
            if not (String.isEmpty s1) then
                if not (String.isEmpty s2) then
                    column

                else
                    ""

            else
                ""

        trimAppendStrings : String -> String -> String
        trimAppendStrings s1 s2 =
            String.trim s1
                |> String.append (String.trim s2 |> addColumn s1)
                |> String.append (String.trim s2)
    in
    List.foldl
        (\line1 line2 ->
            List.foldl trimAppendStrings "" (String.split column line1)
                |> String.append line2
        )
        ""
        linesArr



-- VIEW


view : EditorModel -> ViewParams msg -> Element.Element msg
view model params =
    let
        p =
            machineAttributeRow.padding

        submitButtonActive : Bool
        submitButtonActive =
            if params.active then
                case model.current of
                    Valid _ ->
                        True

                    _ ->
                        False

            else
                False
    in
    Element.column
        [ Element.paddingEach { noPadding | top = p, bottom = p }
        , Element.width Element.fill
        , Font.color Colors.darkGrey
        ]
        [ case model.current of
            NoCheck ->
                case model.source of
                    Valid data ->
                        if model.config.editorOn == True then
                            viewEditor model.source params model.key model.config.id params.active

                        else
                            viewJsonParagraph data

                    _ ->
                        viewTextParagraph model.config.sourceText

            Valid text ->
                if model.config.editorOn == True then
                    viewEditor (Valid text) params model.key model.config.id params.active

                else
                    viewJsonParagraph text

            Invalid text error ->
                if model.config.editorOn == True then
                    viewEditor (Invalid text error) params model.key model.config.id params.active

                else
                    viewJsonParagraph text
        , Element.el
            [ Element.paddingEach { noPadding | top = 10 }
            , Element.width Element.fill
            ]
            (Button.view
                { label = Just model.config.saveButtonLabel
                , buttonType =
                    if submitButtonActive then
                        Just Button.Primary

                    else
                        Just Button.Disabled
                , onClick = params.onSubmit
                , paddingY = Just 10
                , paddingX = Just 30
                , radius = Just 5
                , shadow = Just Colors.darkGrey
                , size = Just Button.Normal
                }
            )
        ]


viewTextParagraph : String -> Element.Element msg
viewTextParagraph text =
    Element.paragraph [] [ Element.text text ]


viewJsonParagraph : String -> Element.Element msg
viewJsonParagraph text =
    Element.html <|
        StyledHtml.toUnstyled <|
            StyledHtml.pre [] [ StyledHtml.text text ]


validatorToStringWithDefault : Validator String -> String -> String
validatorToStringWithDefault valid fallback =
    case valid of
        Valid text ->
            text

        Invalid text _ ->
            text

        NoCheck ->
            fallback


viewEditor : Validator String -> ViewParams msg -> Int -> String -> Bool -> Element.Element msg
viewEditor valid params key id active =
    let
        json =
            validatorToStringWithDefault valid ""

        linesArr : List String
        linesArr =
            String.lines json

        errorLineNumber : Int
        errorLineNumber =
            getErrorLineNumber linesArr valid
    in
    Element.el
        [ Element.width Element.fill
        , Border.solid
        , Border.width 2
        , Border.color Colors.primaryColor
        , Border.rounded 5
        , Element.height (Element.px 400)
        , Element.htmlAttribute <| Html.Attributes.style "overflowX" "hidden"
        , Element.htmlAttribute <| Html.Attributes.style "overflowY" "auto"
        , Element.htmlAttribute <| Html.Attributes.style "scrollbar-width" "thin"
        ]
        (Element.row
            [ Element.height Element.fill
            , Element.width Element.fill
            ]
            [ Element.column
                [ Element.width (Element.fillPortion 2 |> Element.minimum 40)
                , Element.height Element.fill
                , Element.paddingEach
                    { noPadding
                        | left = 3
                        , right = 3
                        , top = textAreaPadding
                        , bottom = textAreaPadding
                    }
                , Background.color Colors.lightGrey
                , Font.color Colors.black
                , Font.size 12
                ]
                (viewLinesNumbers linesArr errorLineNumber)
            , Element.el
                [ Element.width <| Element.fillPortion 26
                , Element.height <| Element.fill
                ]
                (viewTextarea json (not active) params.onChange key id)
            ]
        )


viewLinesNumbers : List String -> Int -> List (Element.Element msg)
viewLinesNumbers linesArr errLineNumber =
    List.indexedMap
        (\index _ ->
            let
                lineHeight =
                    textAreaLineHeight

                lineNum =
                    index + 1
            in
            Element.el
                [ Element.width Element.fill
                , Element.height <| Element.px lineHeight
                ]
                (Element.row
                    [ Element.alignTop
                    , Element.width Element.fill
                    , Element.htmlAttribute <|
                        Html.Attributes.style "line-height" <|
                            String.fromInt lineHeight
                                ++ "px"
                    , Element.height <| Element.px lineHeight
                    ]
                    [ if lineNum == errLineNumber then
                        Element.text "x"
                            |> Element.el
                                [ Font.color Colors.whiteCream
                                , Background.color Colors.dangerColor
                                , Border.color Colors.darkGrey
                                , Border.width 1
                                , Font.size 14
                                , Border.rounded 3
                                , Element.paddingEach { noPadding | left = 2, right = 2 }
                                ]

                      else
                        Element.none
                    , Element.el [ Element.alignRight ] (Element.text <| String.fromInt lineNum)
                    ]
                )
        )
        linesArr


viewTextarea : String -> Bool -> (Bool -> String -> msg) -> Int -> String -> Element.Element msg
viewTextarea output disabled onChange key id =
    Keyed.el
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        ( String.fromInt key
        , Element.html <|
            StyledHtml.toUnstyled <|
                StyledHtml.textarea
                    [ StyledAttributes.css <|
                        List.concat
                            [ [ Css.padding (Css.px <| toFloat textAreaPadding)
                              , Css.lineHeight (Css.px <| toFloat textAreaLineHeight)
                              , Css.height (Css.pct 100)
                              , Css.overflowY Css.hidden
                              , Css.overflowX Css.auto
                              , Css.borderWidth (Css.px 0)
                              , Css.resize Css.none
                              , Css.outline Css.none
                              , Css.property "scrollbar-width" "thin"
                              ]
                            , if not disabled then
                                List.singleton <|
                                    Css.whiteSpace Css.noWrap

                              else
                                []
                            ]
                    , StyledEvents.onInput <| onChange False
                    , StyledAttributes.spellcheck False
                    , StyledAttributes.disabled disabled
                    , StyledAttributes.id id
                    , StyledAttributes.autofocus True

                    --, StyledAttributes.value output
                    ]
                    [ StyledHtml.text output ]
        )


getErrorLineNumber : List String -> Validator String -> Int
getErrorLineNumber linesArr valid =
    let
        errorNumberMatches : List Regex.Match
        errorNumberMatches =
            case valid of
                Invalid _ err ->
                    case err of
                        Decode.Failure reason _ ->
                            Regex.findAtMost
                                1
                                (Maybe.withDefault Regex.never <|
                                    Regex.fromString "\\d+"
                                )
                                reason

                        _ ->
                            []

                _ ->
                    []

        errorNumber : Int
        errorNumber =
            case List.head errorNumberMatches of
                Just obj ->
                    Maybe.withDefault 0 <| String.toInt obj.match

                _ ->
                    0

        symNumList =
            if errorNumber > 0 then
                List.indexedMap
                    (\index line ->
                        if index > 0 then
                            List.foldr
                                (\sym acc -> String.length sym + 1 + acc)
                                0
                                (List.take (index + 1) linesArr)

                        else
                            String.length line
                    )
                    linesArr

            else
                []

        errorLineNumber : Int
        errorLineNumber =
            let
                firstElem : Maybe ( Int, Int )
                firstElem =
                    List.head <|
                        List.filter
                            (\tuple ->
                                errorNumber < Tuple.second tuple
                            )
                            (Array.toIndexedList <|
                                Array.fromList symNumList
                            )
            in
            case firstElem of
                Just tuple ->
                    Tuple.first tuple + 1

                Nothing ->
                    0
    in
    errorLineNumber


jsonObjectView : Json.Field -> Int -> String -> String
jsonObjectView data depth indent =
    let
        wrapByQuotes : String -> String
        wrapByQuotes str =
            "\"" ++ str ++ "\""

        addBreakLine : Int -> String
        addBreakLine d =
            "\n" ++ String.repeat d indent

        addCommaIfNotLast : String -> String
        addCommaIfNotLast v =
            if v /= "" then
                ","

            else
                ""
    in
    case data of
        Json.FieldObj obj ->
            let
                allKeys =
                    Dict.keys obj

                objValue : String -> String
                objValue field =
                    jsonObjectView
                        (Maybe.withDefault Json.FieldNull (Dict.get field obj))
                        (depth + 1)
                        indent

                innerString =
                    List.foldl
                        (\key accum ->
                            addBreakLine (depth + 1)
                                ++ wrapByQuotes key
                                ++ ": "
                                ++ objValue key
                                ++ addCommaIfNotLast accum
                                ++ accum
                        )
                        ""
                        allKeys
            in
            "{" ++ innerString ++ addBreakLine depth ++ "}"

        Json.FieldList list ->
            let
                innerString =
                    List.foldl
                        (\v accum ->
                            addBreakLine (depth + 1)
                                ++ jsonObjectView v (depth + 1) indent
                                ++ addCommaIfNotLast accum
                                ++ accum
                        )
                        ""
                        list
            in
            "[" ++ innerString ++ addBreakLine depth ++ "]"

        Json.FieldString str ->
            wrapByQuotes str

        Json.FieldInt num ->
            wrapByQuotes <| String.fromInt num

        Json.FieldBool bool ->
            wrapByQuotes <|
                if bool == True then
                    "true"

                else
                    "false"

        Json.FieldNull ->
            "null"
