module Components.Grid exposing (view)

import Core.Api.Responsiveness as Responsive
import Core.Config.Colors as Colors
import Core.Config.Layout exposing (boxMargin, noPadding, pageTitle, textShadow)
import Element
    exposing
        ( alignTop
        , fill
        , height
        , htmlAttribute
        , minimum
        , none
        , padding
        , paddingEach
        , text
        , width
        )
import Element.Font as Font
import Html.Attributes exposing (style)


view :
    { children : List (Element.Element msg)
    , message : Maybe (Element.Element msg)
    , screen : Responsive.Type
    , titleText : String
    }
    -> Element.Element msg
view options =
    let
        titlePadding =
            pageTitle.paddingEach

        maxWidth =
            case options.screen of
                Responsive.TabletPortrait ->
                    50

                Responsive.Mobile ->
                    100

                _ ->
                    33

        minMessageHeight =
            case options.screen of
                Responsive.Mobile ->
                    66

                _ ->
                    46
    in
    Element.column
        [ width fill
        , htmlAttribute <| style "margin-left" ((String.fromInt <| negate boxMargin) ++ "px")
        , htmlAttribute <| style "margin-right" ((String.fromInt <| negate boxMargin) ++ "px")
        ]
        [ Element.wrappedRow
            [ width fill
            , Font.color pageTitle.color
            , Font.size pageTitle.size
            , paddingEach { titlePadding | left = boxMargin, right = boxMargin }
            , htmlAttribute <|
                style "text-shadow" <|
                    textShadow
                        ++ Colors.toHexColor Colors.black
            ]
            [ text options.titleText ]
        , Element.wrappedRow
            [ paddingEach { noPadding | left = boxMargin, right = boxMargin }
            , height (fill |> minimum minMessageHeight)
            , width fill
            ]
            [ Maybe.withDefault none options.message
            ]
        , Element.wrappedRow
            [ width fill
            , paddingEach
                { noPadding
                    | top =
                        if List.isEmpty options.children == True then
                            0

                        else
                            boxMargin // 2
                }
            ]
            (List.map
                (\el ->
                    Element.el
                        [ width fill
                        , alignTop
                        , htmlAttribute <| style "flex-basis" (String.fromInt maxWidth ++ "%")
                        , htmlAttribute <| style "max-width" (String.fromInt maxWidth ++ "%")
                        , padding boxMargin
                        ]
                        el
                )
                options.children
            )
        ]
