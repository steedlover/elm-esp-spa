module Components.Link exposing (Size(..), Style(..), view)

import Core.Config.Colors as Colors
import Css
import Element
import Html.Events exposing (onClick)
import Html.Styled exposing (button, text, toUnstyled)
import Html.Styled.Attributes exposing (css, fromUnstyled)


type alias LinkColorSchema =
    { mainColor : Element.Color
    , hoverColor : Element.Color
    }


defaulColorSchema : LinkColorSchema
defaulColorSchema =
    { mainColor = Colors.primaryColorDarker
    , hoverColor = Colors.primaryColor
    }


whiteColorSchema : LinkColorSchema
whiteColorSchema =
    { mainColor = Colors.whiteCream
    , hoverColor = Colors.lightGrey
    }


blackColorSchema : LinkColorSchema
blackColorSchema =
    { mainColor = Colors.black
    , hoverColor = Colors.darkGrey
    }


type Style
    = Default
    | White
    | Black


type Size
    = Small
    | Medium
    | Big


view :
    { label : String
    , click : msg
    , style : Maybe Style
    , size : Maybe Size
    }
    -> Element.Element msg
view options =
    Element.html <|
        toUnstyled <|
            button
                [ fromUnstyled <| onClick options.click
                , css
                    [ Css.color <|
                        Colors.toCssColor (getColorSchema options.style |> .mainColor)
                    , Css.textDecoration Css.underline
                    , Css.border (Css.px 0)
                    , Css.fontSize (Css.px <| getFontSize options.size)
                    , Css.backgroundColor Css.transparent
                    , Css.cursor Css.pointer
                    , Css.padding (Css.px 0)
                    , Css.focus [ Css.outline Css.zero ]
                    , Css.hover
                        [ Css.textDecoration Css.none
                        , Css.color <|
                            Colors.toCssColor (getColorSchema options.style |> .hoverColor)
                        ]
                    ]
                ]
                [ text options.label ]


getColorSchema : Maybe Style -> LinkColorSchema
getColorSchema style =
    case style of
        Just s ->
            case s of
                Default ->
                    defaulColorSchema

                White ->
                    whiteColorSchema

                Black ->
                    blackColorSchema

        Nothing ->
            defaulColorSchema


getFontSize : Maybe Size -> Float
getFontSize size =
    case size of
        Just s ->
            case s of
                Small ->
                    toFloat 16

                Medium ->
                    toFloat 18

                Big ->
                    toFloat 20

        Nothing ->
            toFloat 18
