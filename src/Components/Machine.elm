module Components.Machine exposing (attributeRow)

import Element
import Element.Font as Font
import Core.Config.Layout exposing (noPadding, machineAttributeRow)
import Core.Api.Languages exposing (translate)
import I18Next


attributeRow :
   { key : String
   , val : String
   , translation : Maybe I18Next.Translations
   , extraStylesCommon : List (Element.Attribute msg)
   , extraStylesValue : List (Element.Attribute msg)
   } -> Element.Element msg
attributeRow options =
    let
        p = machineAttributeRow.padding
    in
    Element.row
        (List.concat
            [ [ Element.width Element.fill
              , Element.paddingEach { noPadding | top = p, bottom = p }
              ]
             , options.extraStylesCommon
             ]
        )
        [ Element.el
            [ Font.heavy, Element.paddingEach { noPadding | right = p } ]
            (Element.text <| translate options.translation options.key ++ ":")
        , Element.el
            options.extraStylesValue
            (Element.text <| options.val)
        ]
