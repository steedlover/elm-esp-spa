module Components.Messages exposing (Message(..), show, showCentered)

import Core.Config.Colors as Colors
import Element exposing (fill, htmlAttribute, none, paddingXY, paragraph, row, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html.Attributes exposing (style)


type Message
    = Error Config
    | ErrorMedium Config
    | Success Config
    | Spiner
    | None


type alias Config =
    { title : Maybe String
    , body : String
    , layout : Maybe Bool
    }


show : Message -> Element.Element msg
show message =
    case message of
        Error config ->
            row
                (setAttributes
                    Normal
                    [ Font.color Colors.dangerColor
                    ]
                    (layoutToBool config.layout)
                )
                (List.singleton <| paragraph [] [ text config.body ])

        ErrorMedium config ->
            row
                (setAttributes
                    Medium
                    [ Font.color Colors.dangerColor
                    , htmlAttribute <| style "margin-bottom" "5px"
                    ]
                    (layoutToBool config.layout)
                )
                (List.singleton <| paragraph [] [ text config.body ])

        Success config ->
            row
                (setAttributes
                    Normal
                    [ Font.color Colors.whiteCream
                    ]
                    (layoutToBool config.layout)
                )
                (List.singleton <| paragraph [] [ text config.body ])

        Spiner ->
            text "Loading..."

        None ->
            none


showCentered : Message -> Element.Element msg
showCentered message =
    case message of
        Error config ->
            row
                (setAttributes
                    Normal
                    [ Font.color Colors.dangerColor
                    , htmlAttribute <| style "justify-content" "center"
                    ]
                    (layoutToBool config.layout)
                )
                (List.singleton <| paragraph [] [ text config.body ])

        ErrorMedium config ->
            row
                (setAttributes
                    Medium
                    [ Font.color Colors.dangerColor
                    , htmlAttribute <| style "justify-content" "center"
                    ]
                    (layoutToBool config.layout)
                )
                (List.singleton <| paragraph [] [ text config.body ])

        Success config ->
            row
                (setAttributes
                    Normal
                    [ Font.color Colors.whiteCream
                    , htmlAttribute <| style "justify-content" "center"
                    ]
                    (layoutToBool config.layout)
                )
                (List.singleton <| paragraph [] [ text config.body ])

        Spiner ->
            text "Loading..."

        None ->
            none


setAttributes : MessageSize -> List (Element.Attribute msg) -> Bool -> List (Element.Attribute msg)
setAttributes size list layout =
    let
        baseConfig =
            [ Border.rounded 5
            , Background.color Colors.blackWithOpacity
            , width fill
            ]

        dependOnSize =
            getMessageSizeAttr size
    in
    if layout == True then
        List.concat [ baseConfig, list, dependOnSize ]

    else
        List.concat [ list, dependOnSize ]


layoutToBool : Maybe Bool -> Bool
layoutToBool bool =
    case bool of
        Just val ->
            val

        Nothing ->
            False


type MessageSize
    = Normal
    | Medium


getMessageSizeAttr : MessageSize -> List (Element.Attribute msg)
getMessageSizeAttr size =
    case size of
        Normal ->
            [ Font.size 22, paddingXY 10 20 ]

        Medium ->
            [ Font.size 16, paddingXY 10 10 ]
