module Components.Navbar exposing (view)

import Core.Config.Colors as Colors
import Core.Config.Layout exposing (mainLayout)
import Components.Dropdown as Dropdown
import Element exposing
    ( column
    , fill
    , fillPortion
    , height
    , padding
    , width
    , maximum
    , centerX
    , row
    , htmlAttribute
    )
import Element.Border exposing (shadow)
import Element.Background as Background
import Element.Font as Font
import Html.Attributes exposing (style)


view :
    { userDropdown :
        { state : Dropdown.State
        , config : Dropdown.Config msg
        }
    , languageDropdown :
        { state : Dropdown.State
        , config : Dropdown.Config msg
        }
    }
    -> Element.Element msg
view options =
    row
        [ Background.color Colors.lightGrey
        , Font.color Colors.darkGrey
        , htmlAttribute <| style "z-index" "99"
        , shadow
            { offset = ( 0, 5 )
            , size = 4
            , blur = 15
            , color = Colors.black
            }
        , padding mainLayout.padding
        , width fill
        ]
        [ row
            [ centerX
            , width ( fill |> maximum mainLayout.width )
            ]
            [ column [ height fill, width <| fillPortion 9 ]
                [ Dropdown.view options.userDropdown ]
            , column [ height fill, width <| fillPortion 3 ]
                [ Dropdown.view options.languageDropdown ]
            ]
        ]
