module Components.NotFound exposing (view)


import Element exposing (text)
import Spa.Document exposing (Document)


view : Document msg
view =
    { title = "404"
    , body =
        [ text "The page not found"
        ]
    }
