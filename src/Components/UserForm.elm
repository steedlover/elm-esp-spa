module Components.UserForm exposing (view)

import Components.Button as CustomButton
import Components.Messages as Messages
import Core.Api.Data as Data exposing (Data(..))
import Core.Api.InputField as Field
import Core.Api.User exposing (Credentials)
import Core.Config.Colors as Colors
import Core.Config.Layout exposing (userFormWidth)
import Element
    exposing
        ( centerX
        , centerY
        , column
        , el
        , fill
        , htmlAttribute
        , maximum
        , minimum
        , padding
        , paragraph
        , row
        , spacing
        , text
        , width
        , none
        )
import Element.Background as Background
import Element.Border exposing (rounded, shadow)
import Element.Font as Font
import Element.Input as Input
import Html.Events exposing (on)
import Json.Decode as Json


view :
    { credentials : Data Credentials
    , title : String
    , buttonTitle : String
    , onFormSubmit : msg
    , fields : List (Field.Field msg)
    , error : String
    , globalError : Maybe String
    }
    -> Element.Element msg
view options =
    column
        [ centerX
        , centerY
        , spacing 20
        , width
            ( fill
                |> minimum userFormWidth.min
                |> maximum userFormWidth.max
            )
        ]
        [ case options.globalError of
            Just message ->
                Messages.show
                    ( Messages.Error
                        { title = Nothing
                        , body = message
                        , layout = Just True
                        }
                    )
            Nothing ->
                none
        , el
            [ Background.color Colors.lightGrey
            , padding 20
            , rounded 15
            , width fill
            , shadow
                { offset = ( 0, 0 )
                , size = 3
                , blur = 15
                , color = Colors.black
                }
            , Font.color Colors.white
            ]
            ( column [ width fill, spacing 20 ] <|
                List.concat
                    [ List.singleton <|
                        el
                            [ Font.color Colors.darkGrey
                            ]
                            (text options.title)
                    , List.indexedMap renderField options.fields
                    , List.singleton <|
                        row
                            [ width fill, spacing 20 ]
                            [ CustomButton.view
                                { onClick = options.onFormSubmit
                                , label = Just options.buttonTitle
                                , buttonType = Just CustomButton.Primary
                                , radius = Nothing
                                , size = Just CustomButton.Large
                                , shadow = Just Colors.whiteCream
                                , paddingX = Just 50
                                , paddingY = Just 10
                                }
                            , paragraph
                                [ Font.color Colors.dangerColor ]
                                [ case options.credentials of
                                    Data.Failure _ ->
                                        Messages.show
                                            ( Messages.Error
                                                { title = Nothing
                                                , body = options.error
                                                , layout = Just False
                                                }
                                            )

                                    Data.Loading _ ->
                                        Messages.show Messages.Spiner

                                    _ ->
                                        Messages.show Messages.None
                                ]
                            ]
                    ]
            )
        ]


renderField : Int -> Field.Field msg -> Element.Element msg
renderField index field =
    let
        focused =
            if index == 0 then
                [ Input.focusedOnLoad ]

            else
                []

        defaultInputStyle = List.concat [ listFieldAttributes, focused ]
    in
    case field.type_ of
        Field.Text ->
            Input.text
                (inputAttributes defaultInputStyle field.onEnter)
                { placeholder = Just <| Input.placeholder [] <| text field.label
                , label = Input.labelHidden field.label
                , onChange = field.onInput
                , text = field.value
                }

        Field.Password ->
            Input.newPassword
                (inputAttributes defaultInputStyle field.onEnter)
                { text = field.value
                , label = Input.labelHidden field.label
                , show = False
                , placeholder = Just <| Input.placeholder [] <| text field.label
                , onChange = field.onInput
                }


listFieldAttributes : List (Element.Attribute msg)
listFieldAttributes =
    [ Background.color Colors.whiteCream
    , Font.color Colors.black
    , centerX
    , width fill
    , rounded 10
    , shadow
        { offset = ( 0, 0 )
        , size = 1
        , blur = 8
        , color = Colors.whiteCream
        }
    ]


inputAttributes : List (Element.Attribute msg) -> Maybe msg -> List (Element.Attribute msg)
inputAttributes default keypress =
    List.concat
        [ default
        , case keypress of
            Just a ->
                [ onEnter a ]

            Nothing ->
                []
        ]


onEnter : msg -> Element.Attribute msg
onEnter msg =
    Element.htmlAttribute <|
        on "keyup"
            (Json.field "key" Json.string
                |> Json.andThen
                    (\key ->
                        if key == "Enter" then
                            Json.succeed msg

                        else
                            Json.fail ""
                    )
            )
