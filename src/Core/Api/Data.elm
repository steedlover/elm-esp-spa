module Core.Api.Data exposing (Data(..), ErrorResponse, errorDecoder, expectJson, toMaybe)

import Http
import Json.Decode as Json


type Data value
    = NotAsked
    | Loading (Maybe String)
    | Failure String
    | Success value


type alias ErrorResponse =
    { status : Int
    , message : String
    }


errorDecoder : Json.Decoder ErrorResponse
errorDecoder =
    Json.map2 ErrorResponse
        (Json.field "status" Json.int)
        (Json.field "message" Json.string)


toMaybe : Data value -> Maybe value
toMaybe data =
    case data of
        Success value ->
            Just value

        _ ->
            Nothing


expectJson : (Data value -> msg) -> Json.Decoder value -> Http.Expect msg
expectJson msg decoder =
    Http.expectStringResponse (fromResult >> msg) <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err <|
                        String.join " " [ "Wrong URL", url ]

                Http.Timeout_ ->
                    Err "Request timeout"

                Http.NetworkError_ ->
                    Err "Connection issues"

                Http.BadStatus_ _ body ->
                    case Json.decodeString errorDecoder body of
                        Ok error ->
                            Err <|
                                "Error "
                                    ++ String.fromInt error.status
                                    ++ ". "
                                    ++ "Server response: "
                                    ++ error.message

                        Err _ ->
                            Err "Bad status unknown error"

                Http.GoodStatus_ _ body ->
                    case Json.decodeString decoder body of
                        Ok value ->
                            Ok value

                        Err err ->
                            Err (Json.errorToString err)


fromResult : Result String value -> Data value
fromResult result =
    case result of
        Ok value ->
            Success value

        Err reasons ->
            Failure reasons
