module Core.Api.InputField exposing (FieldType(..), Field)

type FieldType
    = Text
    | Password

type alias Field msg =
    { label : String
    , type_ : FieldType
    , value : String
    , onInput : String -> msg
    , onEnter : Maybe msg
    }
