module Core.Api.Languages exposing
    ( Language
    , LanguageModel
    , languagesDataDecoder
    , checkSharedLangModel
    , setLangResult
    , translate
    , translateWithDefault
    )


import Core.Api.Data exposing (Data(..))
import Dict exposing (Dict)
import I18Next exposing
    ( t
    , Translations
    , Delims(..)
    )
import Json.Decode as Json


type alias LanguageModel =
    { allData : LanguagesData
    , currentData : Maybe Translations
    , all : List Language
    , current : Maybe Language
    }


type alias LanguagesData =
    Dict String Translations


type alias Language =
    { label : String
    , alias : String
    }


languagesDataDecoder : Json.Decoder (Dict String Translations)
languagesDataDecoder =
    Json.dict I18Next.translationsDecoder


setLangResult : Maybe LanguagesData -> Maybe String -> LanguageModel
setLangResult inputData currentLangAlias =
    let
        data =
            Maybe.withDefault Dict.empty inputData

        ( options, selected ) =
            handleLangData data (Maybe.withDefault "en" currentLangAlias)

        selectedLangPackage =
            selected |> Maybe.andThen (\s -> Dict.get s.alias data)
    in
    { allData = data
    , currentData = selectedLangPackage
    , all =
        case selected of
            Just lang ->
                List.filter (\el -> el.alias /= lang.alias) options

            Nothing ->
                options
    , current = selected
    }


handleLangData : LanguagesData -> String -> ( List Language, Maybe Language )
handleLangData data currentLangAlias =
    let
        list =
            List.map
                (\key ->
                    let
                        label =
                            case Dict.get key data of
                                Just package ->
                                    t package "LANG_NAME"

                                Nothing ->
                                    key
                    in
                    { alias = key
                    , label = label
                    }
                )
            <|
                Dict.keys data

        searchForEnglish =
            List.filter (\el -> el.alias == currentLangAlias) list

        selected =
            List.head <|
                if List.isEmpty searchForEnglish == False then
                        searchForEnglish
                else
                    list
    in
    ( list, selected )


translate : Maybe Translations -> String -> String
translate someTranslation key =
    case someTranslation of
        Just package ->
            t package key

        Nothing ->
            key


translateWithDefault : Maybe Translations -> String -> String -> String
translateWithDefault someTranslation key fallback =
    case someTranslation of
        Just package ->
            let
                result =
                    t package key
            in
            if result == key then fallback else result

        Nothing ->
            fallback


checkSharedLangModel : LanguageModel -> Maybe Translations
checkSharedLangModel data =
    data.currentData
