module Core.Api.Machines exposing
    ( Machine
    , Dictionary
    , UpdateObject
    , emptyUpdate
    , list
    , byId
    , decoder
    , implementUpdate
    , updateMachineResponseDecoder
    , sendSignal
    )

import Core.Api.Data as Data exposing (Data(..), expectJson, ErrorResponse, errorDecoder)
import Core.Config.Endpoints exposing (Endpoint(..), buildEndpoint)
import Http
import Dict exposing (Dict)
import Json.Decode as Json
import Json.Encode as Encode
import Core.Utils.Decoder


type alias Dictionary = Dict String Machine


type alias Machine =
    { ip : String
    , mac : String
    , type_ : String
    , static_id : String
    , id : String
    , type_available : Bool
    , error_text : String
    , status : Int
    , signals : List String
    , source : String
    , reset_signals : List String
    , state : String
    , signal_default : String
    , timestamp : Int
    , registered : String
    , updated : String
    }


type alias UpdateMachineResponse =
    { id : String
    , update : UpdateObject
    }


updateMachineResponseDecoder : Json.Decoder UpdateMachineResponse
updateMachineResponseDecoder =
    Json.map2 UpdateMachineResponse
        (Json.field "id" Json.string)
        (Json.field "update" updateObjectDecoder)


type alias UpdateObject =
    { error_text : Maybe String
    , signals : Maybe (List String)
    , state : Maybe String
    , status : Maybe Int
    , timestamp : Maybe Int
    , updated : Maybe String
    }


emptyUpdate : UpdateObject
emptyUpdate =
    UpdateObject
        Nothing
        Nothing
        Nothing
        Nothing
        Nothing
        Nothing


updateObjectDecoder : Json.Decoder UpdateObject
updateObjectDecoder =
    Core.Utils.Decoder.record UpdateObject
        |> Core.Utils.Decoder.withOptional "error_text" Json.string
        |> Core.Utils.Decoder.withOptional "signals" (Json.list Json.string)
        |> Core.Utils.Decoder.withOptional "state" Json.string
        |> Core.Utils.Decoder.withOptional "status" Json.int
        |> Core.Utils.Decoder.withOptional "timestamp" Json.int
        |> Core.Utils.Decoder.withOptional "updated" Json.string


implementUpdate : Machine -> UpdateObject -> Machine
implementUpdate orig upd =
    { orig |
        error_text = Maybe.withDefault orig.error_text upd.error_text
        , signals = Maybe.withDefault orig.signals upd.signals
        , state = Maybe.withDefault orig.state upd.state
        , status = Maybe.withDefault orig.status upd.status
        , timestamp = Maybe.withDefault orig.timestamp upd.timestamp
        , updated = Maybe.withDefault orig.updated upd.updated
    }


decoder : Json.Decoder Machine
decoder =
    Core.Utils.Decoder.record Machine
        |> Core.Utils.Decoder.withField "ip" Json.string
        |> Core.Utils.Decoder.withField "mac" Json.string
        |> Core.Utils.Decoder.withField "type" Json.string
        |> Core.Utils.Decoder.withField "static_id" Json.string
        |> Core.Utils.Decoder.withField "id" Json.string
        |> Core.Utils.Decoder.withField "type_available" Json.bool
        |> Core.Utils.Decoder.withDefault "error_text" Json.string ""
        |> Core.Utils.Decoder.withField "status" Json.int
        |> Core.Utils.Decoder.withDefault "signals" (Json.list Json.string) []
        |> Core.Utils.Decoder.withDefault "source" Json.string ""
        |> Core.Utils.Decoder.withField "reset_signals" (Json.list Json.string)
        |> Core.Utils.Decoder.withField "state" Json.string
        |> Core.Utils.Decoder.withDefault "signal_default" Json.string ""
        |> Core.Utils.Decoder.withDefault "timestamp" Json.int 0
        |> Core.Utils.Decoder.withField "registered" Json.string
        |> Core.Utils.Decoder.withDefault "updated" Json.string ""


list :
    { token : String
    , onResponse : Data (List Machine) -> msg
    }
    -> Cmd msg
list options =
    Http.request
        { method = "get"
        , url = buildEndpoint MachinesList
        , body = Http.emptyBody
        , headers =
            [ Http.header "Authorization" ("Bearer " ++ options.token)
            ]
        , expect = Data.expectJson options.onResponse <|
            Json.field "data" (Json.list decoder)
        , timeout = Nothing
        , tracker = Nothing
        }


byId :
    String ->
    { token : String
    , onResponse : Data Machine -> msg
    }
    -> Cmd msg
byId id options =
    let
        body =
            Encode.object <| List.singleton <| ( "id", Encode.string id )
    in
    Http.request
        { method = "post"
        , url = buildEndpoint MachineById
        , body = Http.jsonBody body
        , headers = [ Http.header "Authorization" ("Bearer " ++ options.token) ]
        , expect = Data.expectJson options.onResponse <|
            Json.field "data" decoder
        , timeout = Nothing
        , tracker = Nothing
        }


sendSignal :
   { id: String
   , signal : String
   , token : String
   , arg : Maybe String
   , onResponse : Data ErrorResponse -> msg
   }
   -> Cmd msg
sendSignal options =
    let
        body =
            List.concat
                [ [ ( "id", Encode.string options.id )
                  , ( "signal", Encode.string options.signal )
                  ]
                , case options.arg of
                    Just v ->
                        List.singleton
                            ( "arg", Encode.string v )
                    _ ->
                        []
                ]
                |> Encode.object
    in
    Http.request
        { method = "post"
        , url = buildEndpoint SendSignal
        , body = Http.jsonBody body
        , headers = [ Http.header "Authorization" ("Bearer " ++ options.token) ]
        , expect = Data.expectJson options.onResponse errorDecoder
        , timeout = Nothing
        , tracker = Nothing
        }
