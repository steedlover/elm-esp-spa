module Core.Api.Responsiveness exposing (Type(..), getScreenType)


type Type
    = Mobile          -- 576
    | TabletPortrait  -- 768
    | TabletLandscape -- 992
    | Desktop         -- 1200
    | BigDesktop


getScreenType : Int -> Type
getScreenType screenWidth =
    if screenWidth < 576 then
        Mobile
    else if screenWidth < 768 then
        TabletPortrait
    else if screenWidth < 992 then
        TabletLandscape
    else if screenWidth < 1200 then
        Desktop
    else
        BigDesktop
