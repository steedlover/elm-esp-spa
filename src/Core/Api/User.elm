module Core.Api.User exposing
    ( User
    , Credentials
    , credentialsDecoder
    , decoder
    , encode
    , authentication
    , verifyToken
    , logout
    , LoginResponseJson
    )

import Http
import Json.Decode as Json
import Json.Encode as Encode
import Core.Config.Endpoints exposing(Endpoint(..), buildEndpoint)
import Core.Api.Data as Data exposing (Data(..), expectJson, ErrorResponse, errorDecoder)

type alias User =
    { email : String
    , username : String
    }


type alias Credentials =
    { user : User
    , token : String
    }



credentialsDecoder : Json.Decoder Credentials
credentialsDecoder =
    Json.map2 Credentials
        (Json.field "user" decoder)
        (Json.field "token" Json.string)


decoder : Json.Decoder User
decoder =
    Json.map2 User
        (Json.field "email" Json.string)
        (Json.field "username" Json.string)


encode : User -> Json.Value
encode user =
    Encode.object
        [ ( "username", Encode.string user.username )
        , ( "email", Encode.string user.email )
        ]


type alias LoginResponseJson =
    { status : Int
    , data : Credentials
    , message : String
    }


decodeLoginResponseBody : Json.Decoder Credentials
decodeLoginResponseBody =
    Json.map2 Credentials
        (Json.field "user" decoder)
        (Json.field "token" Json.string)


decodeLoginResponse : Json.Decoder LoginResponseJson
decodeLoginResponse =
    Json.map3 LoginResponseJson
        (Json.field "status" Json.int)
        (Json.field "data" decodeLoginResponseBody)
        (Json.field "message" Json.string)


verifyToken :
    { token : String
    , onResponse : Data ErrorResponse -> msg
    }
    -> Cmd msg
verifyToken options =
    Http.request
        { method = "get"
        , url = buildEndpoint VerifyToken
        , body = Http.emptyBody
        , headers = [ Http.header "Authorization" ("Bearer " ++ options.token) ]
        , expect = Data.expectJson options.onResponse errorDecoder
        , timeout = Nothing
        , tracker = Nothing
        }


authentication :
   { username : String
   , password : String
   , onResponse : Data LoginResponseJson -> msg
   }
   -> Cmd msg
authentication options =
    let
        body =
            Encode.object
                [ ( "username", Encode.string options.username )
                , ( "password", Encode.string options.password )
                ]
    in
    Http.post
        { url = buildEndpoint Login
        , body = Http.jsonBody body
        , expect = Data.expectJson options.onResponse decodeLoginResponse
        }


logout :
    { token : String
    , onResponse : Data ErrorResponse -> msg
    }
    -> Cmd msg
logout options =
    Http.request
        { method = "get"
        , url = buildEndpoint Logout
        , body = Http.emptyBody
        , headers = [ Http.header "Authorization" ("Bearer " ++ options.token) ]
        , expect = Data.expectJson options.onResponse errorDecoder
        , timeout = Nothing
        , tracker = Nothing
        }
