module Core.Config.Colors exposing
    ( dangerColor
    , primaryColor
    , primaryColorDark
    , primaryColorDarker
    , secondaryColor
    , secondaryColorDark
    , secondaryColorDarker
    , successColor
    , successColorDark
    , infoColor
    , infoColorDark
    , infoColorDarker
    , lightGrey
    , darkGrey
    , white
    , black
    , blackWithOpacity
    , whiteCream
    , whiteCreamWithOpacity
    , skyCyanColor
    , toCssColor
    , toHexColor
    )

import Element exposing (rgb255, toRgb)
import Css
import Hex

toCssColor : Element.Color -> Css.Color
toCssColor color =
    let
        rgb =
            toRgb color
    in
    Css.rgb (floor (rgb.red * 255)) (floor (rgb.green * 255)) (floor (rgb.blue * 255))


toHexColor : Element.Color -> String
toHexColor color =
    let
        rgb = toRgb color
        hexRed = hexElemCheck <| floor (rgb.red * 255)
        hexGreen = hexElemCheck <| floor (rgb.green * 255)
        hexBlue = hexElemCheck <| floor (rgb.blue * 255)
    in
    "#" ++ hexRed ++ hexGreen ++ hexBlue


hexElemCheck : Int -> String
hexElemCheck input =
    let
        hex = Hex.toString input
    in
    if String.length hex == 1 then "0" ++ hex else hex



-- COLORS


dangerColor : Element.Color
dangerColor = Element.rgb255 220 53 69 -- #dc3545

primaryColor : Element.Color
primaryColor = Element.rgb255 0 123 255 -- #007bff

primaryColorDark : Element.Color
primaryColorDark = Element.rgb255 0 105 217 -- #0069d9

primaryColorDarker : Element.Color
primaryColorDarker = Element.rgb255 0 98 204 -- #0069d9

skyCyanColor : Element.Color
skyCyanColor = Element.rgb255 173 216 230 -- #add8e6

secondaryColor : Element.Color
secondaryColor = Element.rgb255 108 117 125 -- #6c757d

secondaryColorDark : Element.Color
secondaryColorDark = Element.rgb255 90 98 104 -- #5a6268

secondaryColorDarker : Element.Color
secondaryColorDarker = Element.rgb255 84 91 98 -- #545b62

successColor : Element.Color
successColor = Element.rgb255 92 184 92 -- #5cb85c

successColorDark : Element.Color
successColorDark = Element.rgb255 78 138 78 -- #4e8a4e

infoColor : Element.Color
infoColor = Element.rgb255 23 162 184 -- #17a2b8

infoColorDark : Element.Color
infoColorDark = Element.rgb255 19 132 150 -- #138496

infoColorDarker : Element.Color
infoColorDarker = Element.rgb255 17 122 139 -- #117a8b

lightGrey : Element.Color
lightGrey = Element.rgb255 163 163 163 -- #a3a3a3

darkGrey : Element.Color
darkGrey = Element.rgb255 54 54 54 --#363636

white : Element.Color
white = Element.rgb255 255 255 255 -- #ffffff

whiteCream : Element.Color
whiteCream = Element.rgb255 255 253 208 --##fffdd0

whiteCreamWithOpacity : Element.Color
whiteCreamWithOpacity = Element.rgba255 255 253 208 0.5

black : Element.Color
black = Element.rgb255 0 0 0

blackWithOpacity : Element.Color
blackWithOpacity = Element.rgba255 0 0 0 0.7
