module Core.Config.Endpoints exposing (Endpoint(..), buildEndpoint)

type Endpoint
    = Login
    | Logout
    | Languages
    | VerifyToken
    | MachinesList
    | MachineById
    | SendSignal


buildEndpoint : Endpoint -> String
buildEndpoint endpoint =
    let
        p =
            case port_ of
                Just val ->
                    ":" ++ String.fromInt val

                Nothing ->
                    "/"
        url =
            case endpoint of
                Login ->
                    "login"

                Logout ->
                    "logout"

                VerifyToken ->
                    "isTokenValid"

                Languages ->
                    "getLanguages"

                MachinesList ->
                    "machineList"

                MachineById ->
                    "machineShow"

                SendSignal ->
                    "machineSend"
    in
    host ++ p ++ "/api/" ++ url

host : String
host = "http://localhost"

port_ : Maybe Int
port_ = Just 3000
