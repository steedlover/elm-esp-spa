module Core.Config.Layout exposing
    ( boxMargin
    , chipIconSize
    , flagsImages
    , machineBlockButton
    , machineAttributeRow
    , mainLayout
    , noPadding
    , pageTitle
    , textShadow
    , userFormWidth
    , lightLayoutStylesList
    )

import Core.Config.Colors as Colors
import Element
import Element.Background as Background
import Element.Border as Border


noPadding :
    { top : Int
    , right : Int
    , bottom : Int
    , left : Int
    }
noPadding =
    { top = 0
    , right = 0
    , bottom = 0
    , left = 0
    }


userFormWidth :
    { min : Int
    , max : Int
    }
userFormWidth =
    { min = 300
    , max = 567
    }


mainLayout :
    { width : Int
    , padding : Int
    }
mainLayout =
    { width = 1024
    , padding = 20
    }


flagsImages :
    { path : String
    , ext : String
    }
flagsImages =
    { path = "/images/flags/"
    , ext = ".jpg"
    }


textShadow : String
textShadow =
    "2px 2px"


boxMargin : Int
boxMargin =
    20


pageTitle :
    { size : Int
    , paddingEach : { top : Int, right : Int, bottom : Int, left : Int }
    , color : Element.Color
    }
pageTitle =
    { size = 32
    , paddingEach = { noPadding | top = 20, bottom = 20 }
    , color = Colors.whiteCream
    }


chipIconSize :
    { width : Int
    , height : Int
    }
chipIconSize =
    { width = 38
    , height = 38
    }


machineBlockButton :
    { radius : Int
    , paddingX : Int
    , paddingY : Int
    }
machineBlockButton =
    { radius = 5
    , paddingY = 10
    , paddingX = 30
    }


machineAttributeRow :
    { padding : Int
    }
machineAttributeRow =
    { padding = 4
    }


lightLayoutStylesList : List (Element.Attribute msg)
lightLayoutStylesList =
    [ Border.rounded 5
    , Background.color Colors.whiteCreamWithOpacity
    , Element.paddingXY (boxMargin // 2) (boxMargin // 2)
    ]
