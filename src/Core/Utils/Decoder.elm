module Core.Utils.Decoder exposing
    ( record
    , withField
    , withOptional
    , withDefault
    )

import Json.Decode as Json



-- DECODING RECORDS


record : fn -> Json.Decoder fn
record =
    Json.succeed


withField :
    String
    -> Json.Decoder field
    -> Json.Decoder (field -> value)
    -> Json.Decoder value
withField fieldName decoder =
    apply (Json.field fieldName decoder)


withDefault :
    String
    -> Json.Decoder field
    -> field
    -> Json.Decoder (field -> value)
    -> Json.Decoder value
withDefault fieldName decoder fallback =
    apply <| optionalDecoder (Json.field fieldName Json.value) decoder fallback


withOptional :
    String
    -> Json.Decoder field
    -> Json.Decoder (Maybe field -> value)
    -> Json.Decoder value
withOptional fieldName decoder =
    apply (Json.maybe (Json.field fieldName decoder))



-- INTERNALS


optionalDecoder : Json.Decoder Json.Value -> Json.Decoder value -> value -> Json.Decoder value
optionalDecoder pathDecoder decoder fallback =
    let
        handleResult input =
            case Json.decodeValue pathDecoder input of
                Ok rawVal ->
                    case Json.decodeValue (Json.oneOf [decoder, Json.null fallback]) rawVal of
                        Ok val ->
                            Json.succeed val

                        Err _ ->
                            Json.succeed fallback

                Err _ ->
                    Json.succeed fallback
    in
    Json.value |>
        Json.andThen handleResult


apply : Json.Decoder field -> Json.Decoder (field -> value) -> Json.Decoder value
apply =
    Json.map2 (|>)
