module Core.Utils.Json exposing (Field(..), decoder)

import Dict exposing (Dict)
import Json.Decode as Json



-- TYPES


type Field
    = FieldNull
    | FieldString String
    | FieldInt Int
    | FieldBool Bool
    | FieldList Arr
    | FieldObj Obj


type alias Arr =
    List Field


type alias Obj =
    Dict String Field



-- DECODERS


decoder : Json.Decoder Field
decoder =
    Json.oneOf
        [ Json.map FieldInt Json.int
        , Json.map FieldBool Json.bool
        , Json.map FieldString Json.string
        , Json.map FieldList decodeArray
        , Json.map FieldObj (Json.lazy (\_ -> decodeObject))
        , Json.null FieldNull
        ]


decodeArray : Json.Decoder Arr
decodeArray =
    Json.list <|
        Json.oneOf
            [ Json.map FieldInt Json.int
            , Json.map FieldString Json.string
            , Json.map FieldBool Json.bool
            , Json.map FieldObj (Json.lazy (\_ -> decodeObject))
            ]


decodeObject : Json.Decoder Obj
decodeObject =
    Json.dict <|
        Json.oneOf
            [ Json.map FieldInt Json.int
            , Json.map FieldBool Json.bool
            , Json.map FieldString Json.string
            , Json.map FieldList decodeArray
            , Json.map FieldObj (Json.lazy (\_ -> decodeObject))
            ]
