module Core.Utils.Route exposing (getRouteByPath, navigate)

import Browser.Navigation as Nav
import Dict exposing (Dict)
import Spa.Generated.Route as Route exposing (Route)


navigate : Nav.Key -> Route -> Cmd msg
navigate key route =
    Nav.pushUrl key (Route.toString route)


getRouteByPath : String -> Maybe Route
getRouteByPath path =
    Dict.get path router



-- Internals


router : Dict String Route
router =
    Dict.fromList
        [ ( "/", Route.Top )
        , ( "list", Route.List )
        , ( "login", Route.Login )
        , ( "logout", Route.Logout )
        , ( "404", Route.NotFound )
        ]
