module Core.Utils.String exposing (capitalize, htmlSpecialCharsDecode, htmlSpecialCharsEncode)



-- INTERNALS


chrmap : List (String, String)
chrmap =
    [ ( "&reg;", "®" )
    , ( "&copy;", "©" )
    , ( "&bull;", "•" )
    , ( "&nbsp;", " " )
    , ( "&rarr;", "→")
    , ( "&#x22;", "\"" )
    , ( "&quot;", "\"" )
    ]



-- FUNCTIONS


capitalize : String -> String
capitalize input =
    let
        firstLetterCapitalized =
            String.left 1 input
                |> String.toUpper
    in
    String.concat
        [ firstLetterCapitalized
        , String.dropLeft 1 input
        ]


htmlSpecialCharsDecode : String -> String
htmlSpecialCharsDecode str =
    let
        replace ( s1, s2 ) src =
            String.join s2 <|
                String.split s1 src
    in
    List.foldl replace str chrmap


htmlSpecialCharsEncode : String -> String
htmlSpecialCharsEncode str =
    let
        replace ( s1, s2 ) src =
            String.join s1 <|
                String.split s2 src
    in
    List.foldl replace str chrmap
