module Core.Utils.Timer exposing (posixToHumanReadable, second)


import Time


second : Int
second = 1000



addLeadingZero : String -> String
addLeadingZero str =
    if String.length str == 1 then
        String.fromInt 0 ++ str
    else
        str


posixToHumanReadable : Time.Posix -> String
posixToHumanReadable time =
    let
        hours   = String.fromInt (Time.toHour   Time.utc time)
        minutes = String.fromInt (Time.toMinute Time.utc time)
        seconds = String.fromInt (Time.toSecond Time.utc time)
    in
    addLeadingZero hours ++ ":" ++ addLeadingZero minutes ++ ":" ++ addLeadingZero seconds
