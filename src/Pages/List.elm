module Pages.List exposing (Model, Msg, Params, page)

import Browser.Navigation exposing (Key)
import Components.Breadcrumbs as Breadcrumbs
import Components.Button as CustomButton
import Components.Grid as Grid
import Components.Link as CustomLink
import Components.Machine
import Components.Messages as Messages
import Core.Api.Data exposing (Data(..), ErrorResponse)
import Core.Api.Languages exposing (checkSharedLangModel, translate)
import Core.Api.Machines as Machines
import Core.Api.Responsiveness as Responsive exposing (Type(..))
import Core.Config.Colors as Colors
import Core.Config.Layout exposing (boxMargin, chipIconSize, machineBlockButton, noPadding)
import Core.Utils.Route exposing (navigate)
import Core.Utils.String as StringExtra
import Core.Utils.Timer as Timer
import Dict
import Element
    exposing
        ( alignRight
        , fill
        , height
        , htmlAttribute
        , image
        , none
        , paddingEach
        , px
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html.Attributes exposing (style)
import I18Next exposing (Translations)
import Json.Decode as Json
import Ports
import Shared
import Spa.Document exposing (Document)
import Spa.Generated.Route as Route exposing (Route)
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)
import Time


page : Page Params Model Msg
page =
    Page.protectedApplication
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    ()


type alias SafeModel =
    { key : Key
    , url : Url Params
    , token : String
    , error : Maybe String
    , errorTimer : Int
    , translation : Maybe Translations
    , screenSize : Responsive.Type
    , machines : Data Machines.Dictionary
    }


type alias Model =
    Maybe SafeModel


init : Shared.Model -> Url Params -> ( SafeModel, Cmd Msg )
init shared url =
    let
        token =
            case shared.credentials of
                Just user ->
                    user.token

                Nothing ->
                    ""
    in
    ( { key = url.key
      , url = url
      , token = token
      , error = Nothing
      , errorTimer = 0
      , translation = checkSharedLangModel shared.languages
      , screenSize = shared.screenSize
      , machines = Loading Nothing
      }
    , Machines.list
        { token = token
        , onResponse = GotMachinesList
        }
    )



-- UPDATE


type Msg
    = GotMachinesList (Data (List Machines.Machine))
    | SendSignal String String
    | GotError (Data ErrorResponse)
    | AddMachine Json.Value
    | RemoveMachine String
    | UpdateMachine Json.Value
    | MachinesTimerTick String Machines.UpdateObject Time.Posix
    | UpdateErrorTimer Int Time.Posix
    | GoToDetails String
    | LinkClicked Route


update : Msg -> SafeModel -> ( SafeModel, Cmd Msg )
update msg model =
    case msg of
        GotMachinesList response ->
            let
                dictionaryMachines =
                    case response of
                        Success data ->
                            Success <|
                                Dict.fromList <|
                                    List.map (\el -> ( el.id, el )) data

                        NotAsked ->
                            NotAsked

                        Loading _ ->
                            Loading Nothing

                        Failure err ->
                            Failure err
            in
            ( { model | machines = dictionaryMachines }, Cmd.none )

        AddMachine json ->
            let
                result =
                    json |> Json.decodeValue Machines.decoder |> Result.toMaybe
            in
            case result of
                Just machine ->
                    case model.machines of
                        Success data ->
                            ( { model
                                | machines =
                                    Success <|
                                        Dict.insert machine.id machine data
                              }
                            , Cmd.none
                            )

                        _ ->
                            ( model, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        RemoveMachine id ->
            case model.machines of
                Success data ->
                    ( { model | machines = Success <| Dict.remove id data }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        UpdateMachine json ->
            let
                result =
                    json
                        |> Json.decodeValue Machines.updateMachineResponseDecoder
                        |> Result.toMaybe
            in
            case result of
                Just updateObj ->
                    let
                        newMachines =
                            attemptToUpdateMachinesDict model.machines updateObj.id updateObj.update
                    in
                    ( { model | machines = newMachines }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        SendSignal id signal ->
            ( model
            , Machines.sendSignal
                { id = id
                , signal = signal
                , token = model.token
                , arg = Nothing
                , onResponse = GotError
                }
            )

        GotError data ->
            let
                error =
                    case data of
                        Failure err ->
                            Just err

                        _ ->
                            Nothing
            in
            ( { model | error = error, errorTimer = 5 }, Cmd.none )

        MachinesTimerTick id updateObj _ ->
            ( { model
                | machines = attemptToUpdateMachinesDict model.machines id updateObj
              }
            , Cmd.none
            )

        UpdateErrorTimer newTime _ ->
            let
                newError =
                    if newTime > 0 then
                        model.error

                    else
                        Nothing
            in
            ( { model | error = newError, errorTimer = newTime }, Cmd.none )

        GoToDetails id ->
            ( model
            , navigate model.key <|
                Route.List__Id_String { id = id }
            )

        LinkClicked route ->
            ( model, navigate model.key route )


attemptToUpdateMachinesDict :
    Data Machines.Dictionary
    -> String
    -> Machines.UpdateObject
    -> Data Machines.Dictionary
attemptToUpdateMachinesDict data id updateObj =
    case data of
        Success machines ->
            Success <|
                Dict.update
                    id
                    (Maybe.map (\el -> Machines.implementUpdate el updateObj))
                    machines

        _ ->
            data


save : SafeModel -> Shared.Model -> Shared.Model
save _ shared =
    shared


load : Shared.Model -> SafeModel -> ( SafeModel, Cmd Msg )
load shared model =
    ( { model
        | screenSize = shared.screenSize
        , translation = checkSharedLangModel shared.languages
      }
    , Cmd.none
    )



-- SUBSCRIPTIONS


subscriptions : SafeModel -> Sub Msg
subscriptions model =
    let
        socketListeners =
            [ Ports.registerMachine AddMachine
            , Ports.removeMachine RemoveMachine
            , Ports.updateMachine UpdateMachine
            ]
    in
    Sub.batch <|
        List.concat
            [ timersSubscriptionsList model.machines
            , socketListeners
            , List.singleton <| errorTimerSubscription model.error model.errorTimer
            ]


errorTimerSubscription : Maybe String -> Int -> Sub Msg
errorTimerSubscription error time =
    case error of
        Nothing ->
            Sub.none

        Just _ ->
            Time.every
                (toFloat Timer.second)
                (UpdateErrorTimer (time - 1))


timersSubscriptionsList : Data Machines.Dictionary -> List (Sub Msg)
timersSubscriptionsList machines =
    let
        emptyUpdateObject =
            Machines.emptyUpdate
    in
    case machines of
        Success data ->
            List.map
                (\elem ->
                    Time.every
                        (toFloat Timer.second)
                        (MachinesTimerTick
                            elem.id
                            { emptyUpdateObject | timestamp = Just (elem.timestamp + 1) }
                        )
                )
                (List.filter (\e -> e.status > 0) (Dict.values data))

        _ ->
            List.singleton Sub.none



-- VIEW


view : SafeModel -> Document Msg
view model =
    { title = translate model.translation "TITLES.MACHINES_PAGE"
    , body =
        [ Element.column
            [ width fill ]
            [ Breadcrumbs.view
                { url = model.url
                , onClick = LinkClicked
                , translation = model.translation
                }
            , Element.row
                [ width fill ]
                [ Grid.view
                    { children =
                        case model.machines of
                            Success machines ->
                                List.map
                                    (\k ->
                                        case Dict.get k machines of
                                            Just m ->
                                                machineBlockView m model.translation

                                            Nothing ->
                                                none
                                    )
                                    (Dict.keys machines)

                            _ ->
                                []
                    , titleText = translate model.translation "TITLES.MACHINES_PAGE"
                    , screen = model.screenSize
                    , message = messageView model
                    }
                ]
            ]
        ]
    }


messageView : SafeModel -> Maybe (Element.Element msg)
messageView model =
    case model.machines of
        Failure error ->
            Just <|
                Messages.show
                    (Messages.Error
                        { title = Nothing
                        , body = error
                        , layout = Just True
                        }
                    )

        Success machines ->
            if Dict.isEmpty machines == True then
                Just <|
                    Messages.show
                        (Messages.Success
                            { title = Nothing
                            , body =
                                translate model.translation "MESSAGES.NO_REGISTERED_DEVICES"
                            , layout = Just True
                            }
                        )

            else
                case model.error of
                    Just err ->
                        Just <|
                            Messages.show
                                (Messages.ErrorMedium
                                    { title = Nothing
                                    , body = err
                                    , layout = Just True
                                    }
                                )

                    Nothing ->
                        Nothing

        Loading _ ->
            Just <|
                Messages.show
                    Messages.Spiner

        _ ->
            Nothing


machineBlockView : Machines.Machine -> Maybe I18Next.Translations -> Element.Element Msg
machineBlockView machine t =
    Element.column
        [ Background.color Colors.whiteCream
        , width fill
        , Border.rounded 15
        , htmlAttribute <| style "overflow" "hidden"
        , Border.shadow
            { offset = ( 0, 0 )
            , size = 3
            , blur = 15
            , color = Colors.black
            }
        ]
        [ machineBlockHeader machine t
        , Element.column
            [ width fill
            , paddingEach
                { noPadding
                    | left = boxMargin
                    , right = boxMargin
                    , top = boxMargin // 2
                    , bottom = boxMargin
                }
            ]
            [ Components.Machine.attributeRow
                { key = "MACHINE_ITEM.ID"
                , val = machine.id
                , translation = t
                , extraStylesCommon = [ Font.color Colors.secondaryColorDark ]
                , extraStylesValue = []
                }
            , Components.Machine.attributeRow
                { key = "MACHINE_ITEM.TYPE"
                , val = machine.type_
                , translation = t
                , extraStylesCommon = [ Font.color Colors.secondaryColorDark ]
                , extraStylesValue = []
                }
            , Components.Machine.attributeRow
                { key = "MACHINE_ITEM.UPTIME"
                , val =
                    Timer.posixToHumanReadable <|
                        Time.millisToPosix (machine.timestamp * Timer.second)
                , translation = t
                , extraStylesCommon = [ Font.color Colors.secondaryColorDark ]
                , extraStylesValue = []
                }
            , Components.Machine.attributeRow
                { key = "MACHINE_ITEM.STATE"
                , val = StringExtra.capitalize machine.state
                , translation = t
                , extraStylesCommon = [ Font.color Colors.secondaryColorDark ]
                , extraStylesValue = []
                }
            , Element.row
                [ width fill
                , paddingEach { noPadding | top = 4 }
                ]
                [ CustomLink.view
                    { label = "[" ++ translate t "MACHINE_ITEM.DETAILS_LINK" ++ "]"
                    , click = GoToDetails machine.id
                    , style = Nothing
                    , size = Just CustomLink.Small
                    }
                ]
            , if machine.status > 0 then
                machineSignalsBlock machine t

              else
                none
            ]
        ]


machineBlockHeader : Machines.Machine -> Maybe I18Next.Translations -> Element.Element msg
machineBlockHeader machine t =
    Element.row
        [ Background.color Colors.skyCyanColor
        , Border.widthEach { noPadding | bottom = 1 }
        , Border.color Colors.secondaryColor
        , paddingEach
            { noPadding
                | left = boxMargin
                , right = boxMargin
                , top = boxMargin // 2
                , bottom = boxMargin // 2
            }
        , width fill
        ]
        [ if String.isEmpty machine.error_text then
            image
                [ width <| px chipIconSize.width
                , height <| px chipIconSize.height
                ]
                { src = "/images/microchip.svg"
                , description = ""
                }

          else
            Element.el
                [ Font.color Colors.dangerColor
                , Font.bold
                , paddingEach
                    { noPadding
                        | top = chipIconSize.height // 3
                        , bottom = chipIconSize.height // 3
                    }
                ]
                (Element.text machine.error_text)
        , if machine.status > 0 then
            image
                [ alignRight
                , width <| px <| round (toFloat chipIconSize.width * 1.2)
                , height <| px <| round (toFloat chipIconSize.height * 1.2)
                ]
                { src = "/images/online.svg"
                , description = translate t "MACHINE_DETAILS.ONLINE"
                }

          else
            image
                [ alignRight
                , width <| px <| round (toFloat chipIconSize.width * 0.8)
                , height <| px <| round (toFloat chipIconSize.height * 0.8)
                ]
                { src = "/images/offline.svg"
                , description = translate t "MACHINE_DETAILS.OFFLINE"
                }
        ]


machineSignalsBlock : Machines.Machine -> Maybe Translations -> Element.Element Msg
machineSignalsBlock machine t =
    Element.wrappedRow
        [ width fill
        , spacing 5
        , paddingEach { noPadding | top = boxMargin }
        ]
        (List.concat
            [ List.indexedMap
                (\i sig ->
                    CustomButton.view
                        { onClick = SendSignal machine.id sig
                        , label = Just <| StringExtra.capitalize sig
                        , buttonType =
                            Just <|
                                if i == 0 then
                                    CustomButton.Primary

                                else
                                    CustomButton.Secondary
                        , radius = Just machineBlockButton.radius
                        , size = Just CustomButton.Normal
                        , shadow = Nothing
                        , paddingY = Just machineBlockButton.paddingY
                        , paddingX = Just machineBlockButton.paddingX
                        }
                )
                machine.signals
            , List.singleton <|
                CustomButton.view
                    { onClick = SendSignal machine.id "reset"
                    , label = Just <| translate t "MACHINE_ITEM.RESET"
                    , buttonType = Just CustomButton.Danger
                    , radius = Just machineBlockButton.radius
                    , size = Just CustomButton.Normal
                    , shadow = Nothing
                    , paddingY = Just machineBlockButton.paddingY
                    , paddingX = Just machineBlockButton.paddingX
                    }
            ]
        )
