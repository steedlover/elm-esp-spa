module Pages.List.Id_String exposing (Model, Msg, Params, page)

import Browser.Navigation exposing (Key)
import Components.Breadcrumbs as Breadcrumbs
import Components.Editor as Editor
import Components.Grid as Grid
import Components.Machine
import Components.Messages as Messages
import Core.Api.Data exposing (Data(..), ErrorResponse)
import Core.Api.Languages exposing (checkSharedLangModel, translate)
import Core.Api.Machines as Machines
import Core.Api.Responsiveness as Responsive exposing (Type(..))
import Core.Config.Colors as Colors
import Core.Config.Layout as Layout
import Core.Utils.Route exposing (navigate)
import Core.Utils.String as ExtraString
import Element exposing (fill, width)
import Element.Font as Font
import Element.Lazy as Lazy
import I18Next exposing (Translations)
import Json.Decode as Json
import Ports
import Shared
import Spa.Document exposing (Document)
import Spa.Generated.Route exposing (Route)
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)


type alias Params =
    { id : String }


type alias SafeModel =
    { key : Key
    , url : Url Params
    , token : String
    , machine : Data Machines.Machine
    , translation : Maybe Translations
    , screenSize : Responsive.Type
    , editor : Maybe Editor.EditorModel
    }


type Msg
    = LinkClicked Route
    | GotMachine (Data Machines.Machine)
    | AddMachine Json.Value
    | RemoveMachine String
    | UpdateMachine Json.Value
    | EditorChange Bool String
    | EditorSubmit
    | EditorDisabledSubmit
    | GotError (Data ErrorResponse)


type alias Model =
    Maybe SafeModel


page : Page Params Model Msg
page =
    Page.protectedApplication
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , load = load
        , save = save
        }


editorTextareaId : String
editorTextareaId =
    "editortextarea"



-- INIT


init : Shared.Model -> Url Params -> ( SafeModel, Cmd Msg )
init shared url =
    let
        token =
            case shared.credentials of
                Just user ->
                    user.token

                Nothing ->
                    ""
    in
    ( { key = url.key
      , url = url
      , token = token
      , machine = Loading Nothing
      , translation = checkSharedLangModel shared.languages
      , screenSize = shared.screenSize
      , editor = Nothing
      }
    , Machines.byId
        url.params.id
        { token = token
        , onResponse = GotMachine
        }
    )



-- SUBSCRIPTIONS


subscriptions : SafeModel -> Sub Msg
subscriptions _ =
    Sub.batch
        [ Ports.registerMachine AddMachine
        , Ports.removeMachine RemoveMachine
        , Ports.updateMachine UpdateMachine
        , Ports.setNewTextareaText (EditorChange True)
        ]


save : SafeModel -> Shared.Model -> Shared.Model
save _ shared =
    shared


load : Shared.Model -> SafeModel -> ( SafeModel, Cmd Msg )
load shared model =
    ( { model
        | screenSize = shared.screenSize
        , translation = checkSharedLangModel shared.languages
      }
    , Cmd.none
    )



-- UPDATE


update : Msg -> SafeModel -> ( SafeModel, Cmd Msg )
update msg model =
    case msg of
        EditorChange rerender newText ->
            let
                updateFn =
                    if rerender == True then
                        Editor.updateAndRerender

                    else
                        Editor.update

                editorUpdatedModel =
                    case model.editor of
                        Just editor ->
                            Just <| updateFn editor newText

                        Nothing ->
                            model.editor

                cmd =
                    case model.editor of
                        Just editor ->
                            if rerender == True then
                                Ports.addTextareaListener editor.config.id

                            else
                                Cmd.none

                        _ ->
                            Cmd.none
            in
            ( { model | editor = editorUpdatedModel }, cmd )

        EditorDisabledSubmit ->
            ( model, Cmd.none )

        EditorSubmit ->
            let
                current =
                    case model.editor of
                        Just editor ->
                            Editor.validatorToStringWithDefault editor.current ""

                        _ ->
                            ""

                id =
                    case model.machine of
                        Success machine ->
                            machine.id

                        _ ->
                            ""
            in
            ( model
            , Machines.sendSignal
                { id = id
                , signal = "new_config"
                , token = model.token
                , arg = Just current
                , onResponse = GotError
                }
            )

        GotError data ->
            let
                ( updateMachine, cmd ) =
                    case data of
                        Failure err ->
                            ( Failure err, Cmd.none )

                        _ ->
                            ( model.machine, navigate model.url.key Spa.Generated.Route.List )
            in
            ( { model | machine = updateMachine }, cmd )

        LinkClicked route ->
            ( model, navigate model.url.key route )

        GotMachine data ->
            let
                newEditor =
                    case data of
                        Success machine ->
                            Just <|
                                Editor.create
                                    { editorOn = True
                                    , id = editorTextareaId
                                    , sourceText = ExtraString.htmlSpecialCharsDecode machine.source
                                    , saveButtonLabel =
                                        translate model.translation "MACHINE_DETAILS.SAVE_CONFIG_BUTTON"
                                    }

                        _ ->
                            model.editor
            in
            ( { model | machine = data, editor = newEditor }, Ports.addTextareaListener editorTextareaId )

        UpdateMachine json ->
            let
                updateObj =
                    json |> Json.decodeValue Machines.updateMachineResponseDecoder |> Result.toMaybe

                newMachine =
                    case updateObj of
                        Just newVal ->
                            case model.machine of
                                Success data ->
                                    if data.id == model.url.params.id then
                                        Success <| Machines.implementUpdate data newVal.update

                                    else
                                        model.machine

                                _ ->
                                    model.machine

                        Nothing ->
                            model.machine
            in
            ( { model | machine = newMachine }, Cmd.none )

        RemoveMachine id ->
            let
                newMachine =
                    case model.machine of
                        Success data ->
                            if data.id == id then
                                NotAsked

                            else
                                model.machine

                        _ ->
                            model.machine
            in
            ( { model | machine = newMachine }, Cmd.none )

        AddMachine json ->
            let
                updateObj =
                    json |> Json.decodeValue Machines.decoder |> Result.toMaybe

                newMachine =
                    case updateObj of
                        Just newVal ->
                            case model.machine of
                                Success data ->
                                    if data.id == model.url.params.id then
                                        Success <| newVal

                                    else
                                        model.machine

                                _ ->
                                    model.machine

                        Nothing ->
                            model.machine
            in
            ( { model | machine = newMachine }, Cmd.none )



-- VIEW


view : SafeModel -> Document Msg
view model =
    { title = translate model.translation "TITLES.MACHINE_DETAILS_PAGE"
    , body =
        [ Element.column
            [ width fill ]
            [ Breadcrumbs.view
                { url = model.url
                , onClick = LinkClicked
                , translation = model.translation
                }
            , Element.row
                [ Element.width Element.fill ]
                [ Grid.view
                    { titleText = translate model.translation "TITLES.MACHINE_DETAILS_PAGE"
                    , children = []
                    , message = messageView model
                    , screen = model.screenSize
                    }
                ]
            , Lazy.lazy mainContentView model
            ]
        ]
    }


messageView : SafeModel -> Maybe (Element.Element msg)
messageView model =
    case model.machine of
        Failure err ->
            Just <|
                Messages.show
                    (Messages.ErrorMedium
                        { title = Nothing
                        , body = err
                        , layout = Just True
                        }
                    )

        _ ->
            case model.editor of
                Just editable ->
                    case editable.current of
                        Editor.Invalid _ error ->
                            case error of
                                Json.Failure reason _ ->
                                    Just <|
                                        Messages.show
                                            (Messages.ErrorMedium
                                                { title = Nothing
                                                , body = reason
                                                , layout = Just True
                                                }
                                            )

                                _ ->
                                    Nothing

                        _ ->
                            Nothing

                Nothing ->
                    Nothing


mainContentView : SafeModel -> Element.Element Msg
mainContentView model =
    case model.machine of
        Success obj ->
            Element.column
                (List.concat
                    [ Layout.lightLayoutStylesList
                    , [ Element.width Element.fill
                      , Font.size 18
                      ]
                    ]
                )
                [ Components.Machine.attributeRow
                    { key = "MACHINE_DETAILS.ID"
                    , val = obj.id
                    , translation = model.translation
                    , extraStylesCommon = [ Font.color Colors.darkGrey ]
                    , extraStylesValue = []
                    }
                , Components.Machine.attributeRow
                    { key = "MACHINE_DETAILS.STATUS"
                    , val =
                        if obj.status <= 0 then
                            translate model.translation "MACHINE_DETAILS.OFFLINE"

                        else
                            translate model.translation "MACHINE_DETAILS.ONLINE"
                    , translation = model.translation
                    , extraStylesCommon = [ Font.color Colors.darkGrey ]
                    , extraStylesValue =
                        [ Font.color <|
                            if obj.status <= 0 then
                                Colors.dangerColor

                            else
                                Colors.successColorDark
                        ]
                    }
                , case model.editor of
                    Just editorModel ->
                        Editor.view editorModel
                            { active = obj.status > 0
                            , onChange = EditorChange
                            , onSubmit =
                                if obj.status > 0 then
                                    EditorSubmit

                                else
                                    EditorDisabledSubmit
                            }

                    Nothing ->
                        Element.none
                ]

        Loading _ ->
            Messages.show
                Messages.Spiner

        _ ->
            Element.none
