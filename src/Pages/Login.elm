module Pages.Login exposing (Params, Model, Msg, page)

import Shared
import Ports
import Spa.Document exposing (Document)
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)
import Spa.Generated.Route as Route
import Browser.Navigation exposing (Key)
import Core.Api.User as User exposing (Credentials, LoginResponseJson, authentication)
import Core.Api.InputField as Field
import Core.Api.Data as Data exposing (Data(..), toMaybe)
import Core.Utils.Route exposing (navigate)
import Element exposing (el, width, height, fill, centerY)
import Components.UserForm as UserForm
import Components.Dropdown as Dropdown exposing (State(..))
import I18Next exposing (Translations)
import Core.Api.Languages exposing (translate, checkSharedLangModel)


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    ()


type LoginFields
    = Username
    | Password


type alias Model =
    { credentials : Data Credentials
    , loggedIn : Bool
    , key : Key
    , username : String
    , password : String
    , error : String
    , translation : Maybe Translations
    , globalErrorMessage : Maybe String
    }


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared { key } =
    (
        Model (
            ( case shared.credentials of
                Just credentials ->
                    Data.Success credentials

                Nothing ->
                    Data.NotAsked
            )
        )
        False
        key
        ""
        ""
        ""
        (checkSharedLangModel shared.languages)
        shared.errorMessage
    , Cmd.none )



-- UPDATE


type Msg
    = Updated LoginFields String
    | Submit
    | GotLogin (Data LoginResponseJson)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Updated Username name ->
            ( { model | username = name }, Cmd.none )

        Updated Password pass ->
            ( { model | password = pass }, Cmd.none )

        Submit ->
            ( { model | credentials = Data.Loading Nothing }
            , User.authentication
                { username = model.username
                , password = model.password
                , onResponse = GotLogin
                }
            )

        GotLogin (Failure reason) ->
            ( { model | credentials = Failure reason, error = reason }, Cmd.none )

        GotLogin (Success data) ->
            ( { model | credentials = Success data.data, loggedIn = True }
            , Cmd.batch
                [ Ports.saveUser data.data
                , navigate model.key Route.Top
                ]
            )

        GotLogin _ ->
            ( model, Cmd.none )


save : Model -> Shared.Model -> Shared.Model
save model shared =
    if model.loggedIn == True then
        case Data.toMaybe model.credentials of
            Just credentials ->
                { shared | credentials = Just credentials, dropdowns = Dropdown.AllClosed }

            Nothing ->
                shared
    else
        shared


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load _ model =
    ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = translate model.translation "TITLES.LOGIN_PAGE"
    , body =
        [ el
            [ width fill, height fill, centerY ]
            ( UserForm.view
                { credentials = model.credentials
                , title = translate model.translation "USER_FORM_TITLE"
                , buttonTitle = translate model.translation "USER_FORM_BUTTON_TEXT"
                , onFormSubmit = Submit
                , globalError = model.globalErrorMessage
                , error = model.error
                , fields =
                    [ { label = translate model.translation "USER_FORM_NAME_PLACEHOLDER"
                        , type_ = Field.Text
                        , value = model.username
                        , onInput = Updated Username
                        , onEnter = Just Submit
                      }
                    , { label = translate model.translation "USER_FORM_PASSWORD_PLACEHOLDER"
                        , type_ = Field.Password
                        , value = model.password
                        , onInput = Updated Password
                        , onEnter = Just Submit
                      }
                    ]
                }
            )
        ]
    }
