module Pages.Logout exposing (Params, Model, Msg, page)

import Shared
import Ports
import Browser.Navigation exposing (Key)
import Spa.Document exposing (Document)
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)
import Core.Api.Data exposing (Data(..), ErrorResponse)
import Core.Api.User as User exposing (Credentials)
import Element exposing (none)
import Components.Messages as Messages
import Components.Dropdown as Dropdown exposing (State(..))
import I18Next exposing (Translations)
import Core.Api.Languages exposing (translate, checkSharedLangModel)


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    ()


type alias Model =
    { key : Key
    , credentials : Maybe Credentials
    , message : Data String
    , translation : Maybe Translations
    }


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared _ =
    let
        newTranslation =
            checkSharedLangModel shared.languages
    in
    case shared.credentials of
        Just cred ->
            ( { key = shared.key
              , credentials = Just cred
              , message = Loading Nothing
              , translation = newTranslation
              }
            , User.logout
                { token = cred.token
                , onResponse = LogoutRequest
                }
            )

        Nothing ->
            ( { key = shared.key
              , credentials = shared.credentials
              , message = Failure (translate newTranslation "WRONG_VISIT")
              , translation = newTranslation
              }
            , Cmd.none
            )



-- UPDATE


type Msg
    = LogoutRequest (Data ErrorResponse)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LogoutRequest (Success res) ->
            ( { model
                | credentials = Nothing
                  , message = Success res.message
              }
            , Ports.removeUser )

        LogoutRequest (Failure err) ->
            ( { model | message = Failure err }, Cmd.none )

        LogoutRequest _ ->
            ( model, Cmd.none )


save : Model -> Shared.Model -> Shared.Model
save model shared =
    { shared | credentials = model.credentials, dropdowns = Dropdown.AllClosed }


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load _ model =
    ( model, Cmd.none)


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = translate model.translation "TITLES.LOGOUT_PAGE"
    , body =
        [ viewMessage model.message
        ]
    }


viewMessage : Data String -> Element.Element msg
viewMessage msg =
    case msg of
        Success str ->
            Messages.show
                ( Messages.Success
                    { title = Nothing
                    , body = str
                    , layout = Just True
                    }
                )

        Failure err ->
            Messages.show
                ( Messages.Error
                    { title = Nothing
                    , body = err
                    , layout = Just True
                    }
                )

        Loading _ ->
            Messages.show Messages.Spiner

        NotAsked ->
            Messages.show Messages.None
