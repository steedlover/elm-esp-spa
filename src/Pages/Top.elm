module Pages.Top exposing (Model, Msg, Params, page)

import Browser.Navigation exposing (Key)
import Components.Grid as Grid
import Components.Link as CustomLink
import Core.Api.Languages exposing (checkSharedLangModel, translate)
import Core.Api.Responsiveness as Responsive exposing (Type(..))
import Core.Config.Colors as Colors
import Core.Config.Layout exposing (noPadding)
import Core.Utils.Route exposing (navigate)
import Element
    exposing
        ( centerX
        , fill
        , image
        , none
        , paddingEach
        , width
        )
import Element.Background as Background
import Element.Border exposing (rounded, shadow)
import I18Next exposing (Translations)
import Shared
import Spa.Document exposing (Document)
import Spa.Generated.Route as Route
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)


type alias Params =
    ()


type alias SafeModel =
    { key : Key
    , translation : Maybe Translations
    , screenSize : Responsive.Type
    }


type Msg
    = LinkClicked Route.Route


type alias Model =
    Maybe SafeModel


page : Page Params Model Msg
page =
    Page.protectedApplication
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , load = load
        , save = save
        }



-- INIT


init : Shared.Model -> Url Params -> ( SafeModel, Cmd Msg )
init shared url =
    ( { key = url.key
      , translation = checkSharedLangModel shared.languages
      , screenSize = shared.screenSize
      }
    , Cmd.none
    )


subscriptions : SafeModel -> Sub Msg
subscriptions _ =
    Sub.none


save : SafeModel -> Shared.Model -> Shared.Model
save _ shared =
    shared


load : Shared.Model -> SafeModel -> ( SafeModel, Cmd Msg )
load shared model =
    ( { model
        | screenSize = shared.screenSize
        , translation = checkSharedLangModel shared.languages
      }
    , Cmd.none
    )



-- UPDATE


update : Msg -> SafeModel -> ( SafeModel, Cmd Msg )
update msg model =
    case msg of
        LinkClicked route ->
            ( model, navigate model.key route )



-- VIEW


view : SafeModel -> Document Msg
view model =
    { title = translate model.translation "TITLES.HOME_PAGE"
    , body =
        [ Grid.view
            { children =
                [ menuElem
                    { text = translate model.translation "HOME_PAGE.MACHINES_LIST"
                    , onClick = LinkClicked Route.List
                    , icon = "/images/network.svg"
                    }
                ]
            , screen = model.screenSize
            , titleText = translate model.translation "TITLES.HOME_PAGE"
            , message = Nothing
            }
        ]
    }


menuElem :
    { text : String
    , onClick : msg
    , icon : String
    }
    -> Element.Element msg
menuElem options =
    Element.column
        [ Background.color Colors.whiteCream
        , width fill
        , paddingEach { top = 30, right = 30, bottom = 20, left = 30 }
        , rounded 15
        , shadow
            { offset = ( 0, 0 )
            , size = 3
            , blur = 15
            , color = Colors.black
            }
        ]
        [ image
            [ width fill ]
            { src = options.icon
            , description = options.text
            }
        , Element.el
            [ paddingEach { noPadding | top = 20 }
            , centerX
            ]
            (CustomLink.view
                { label = options.text
                , click = options.onClick
                , style = Nothing
                , size = Just CustomLink.Medium
                }
            )
        ]
