port module Ports exposing
    ( addTextareaListener
    , registerMachine
    , removeMachine
    , removeUser
    , saveDefaultLanguage
    , saveUser
    , setNewTextareaText
    , updateMachine
    )

import Core.Api.User as User exposing (encode)
import Json.Decode as Json
import Json.Encode as Encode


port outgoing :
    { tag : String
    , data : Json.Value
    }
    -> Cmd msg


port registerMachine : (Json.Value -> msg) -> Sub msg


port removeMachine : (String -> msg) -> Sub msg


port updateMachine : (Json.Value -> msg) -> Sub msg


port setNewTextareaText : (String -> msg) -> Sub msg


saveUser : User.Credentials -> Cmd msg
saveUser credentials =
    outgoing
        { tag = "saveUser"
        , data =
            Encode.object
                [ ( "user", encode credentials.user )
                , ( "token", Encode.string credentials.token )
                ]
        }


removeUser : Cmd msg
removeUser =
    outgoing
        { tag = "removeUser"
        , data = Encode.null
        }


saveDefaultLanguage : String -> Cmd msg
saveDefaultLanguage lang =
    outgoing
        { tag = "saveDefaultLanguage"
        , data = Encode.string lang
        }


addTextareaListener : String -> Cmd msg
addTextareaListener id =
    outgoing
        { tag = "addTextareaListener"
        , data = Encode.string id
        }
