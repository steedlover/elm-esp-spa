port module Shared exposing
    ( Flags
    , Model
    , Msg
    , init
    , subscriptions
    , update
    , view
    )

import Browser.Navigation exposing (Key)
import Components.Dropdown as Dropdown exposing (Align(..), AllDropdowns(..), State(..))
import Components.Link as CustomLink exposing (Style(..))
import Components.Navbar as Navbar
import Core.Api.Data exposing (Data(..))
import Core.Api.Languages
    exposing
        ( Language
        , LanguageModel
        , languagesDataDecoder
        , setLangResult
        , translate
        )
import Core.Api.Responsiveness as Screen exposing (Type(..), getScreenType)
import Core.Api.User as User exposing (credentialsDecoder)
import Core.Config.Layout exposing (flagsImages, mainLayout)
import Core.Utils.Route exposing (navigate)
import Element
    exposing
        ( centerX
        , el
        , fill
        , height
        , htmlAttribute
        , image
        , inFront
        , none
        , padding
        , paragraph
        , px
        , row
        , spacing
        , text
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Events as Event
import Html.Attributes exposing (style)
import I18Next exposing (Translations)
import Json.Decode as Json
import Ports
import Spa.Document exposing (Document)
import Spa.Generated.Route as Route
import Url exposing (Url)



-- INIT


type alias Flags =
    Json.Value


type alias Model =
    { url : Url
    , key : Key
    , credentials : Maybe User.Credentials
    , screenSize : Screen.Type
    , languages : LanguageModel
    , dropdowns : Dropdown.AllDropdowns
    , errorMessage : Maybe String
    }


init : Flags -> Url -> Key -> ( Model, Cmd Msg )
init json url key =
    let
        credentials =
            json |> Json.decodeValue credentialsDecoder |> Result.toMaybe

        dropdowns =
            Dropdown.AllClosed

        screenSize =
            case json |> Json.decodeValue (Json.field "screenWidth" Json.int) of
                Result.Ok val ->
                    getScreenType val

                Result.Err _ ->
                    getScreenType 1200

        languages =
            setLangResult
                (json
                    |> Json.decodeValue (Json.field "languages" languagesDataDecoder)
                    |> Result.toMaybe
                )
                (json
                    |> Json.decodeValue (Json.field "defLang" Json.string)
                    |> Result.toMaybe
                )

        errorMessage =
            json |> Json.decodeValue (Json.field "errorMessage" Json.string) |> Result.toMaybe
    in
    ( Model url key credentials screenSize languages dropdowns errorMessage, Cmd.none )


userDropdownConfig : Maybe User.Credentials -> Maybe Translations -> (Msg -> a) -> Dropdown.Config a
userDropdownConfig credentials translation toMsg =
    { fallback =
        CustomLink.view
            { label = translate translation "DROPDOWN_LOGIN_LINK_TEXT"
            , click = toMsg <| LinkClicked Route.Login
            , style = Nothing
            , size = Just CustomLink.Medium
            }
    , rootOption =
        case credentials of
            Just obj ->
                Just
                    (row [ spacing 10 ]
                        [ image [ width <| px 30, height <| px 30 ]
                            { description = "User Logo"
                            , src = "/images/user-logo.png"
                            }
                        , text obj.user.username
                        ]
                    )

            _ ->
                Nothing
    , options =
        case credentials of
            Just obj ->
                Just
                    [ paragraph [] [ text <| obj.user.email ]
                    , Element.row
                        []
                        [ CustomLink.view
                            { label = translate translation "DROPDOWN_LOGOUT_LINK_TEXT"
                            , click = toMsg <| LinkClicked Route.Logout
                            , style = Just CustomLink.White
                            , size = Just CustomLink.Medium
                            }
                        ]
                    ]

            _ ->
                Nothing
    , align = Nothing
    , toggle = (toMsg << DropdownToggle) Dropdown.UserMenuOpened
    }


languageDropdownConfig :
    Maybe Language
    -> List Language
    -> (Msg -> a)
    -> Dropdown.Config a
languageDropdownConfig current all toMsg =
    let
        root =
            case current of
                Just language ->
                    Just <|
                        row [ spacing 10 ]
                            [ image [ width <| px 30, height <| px 20 ]
                                { description = "Switch to " ++ language.label
                                , src = flagsImages.path ++ language.alias ++ flagsImages.ext
                                }
                            , text language.label
                            ]

                _ ->
                    Nothing

        languages =
            if List.length all > 0 then
                Just <|
                    List.map
                        (\lang ->
                            row
                                [ spacing 10 ]
                                [ image
                                    [ width <| px 30
                                    , height <| px 20
                                    , htmlAttribute <| style "margin-right" "10px"
                                    ]
                                    { description = "Switch to " ++ lang.label
                                    , src = flagsImages.path ++ lang.alias ++ flagsImages.ext
                                    }
                                , CustomLink.view
                                    { label = lang.label
                                    , click = toMsg <| NewLanguageSelected lang.alias
                                    , style = Just CustomLink.White
                                    , size = Just CustomLink.Medium
                                    }
                                ]
                        )
                        all

            else
                Nothing
    in
    { fallback = none
    , rootOption = root
    , options = languages
    , align = Just Dropdown.Right
    , toggle = (toMsg << DropdownToggle) Dropdown.LanguageMenuOpened
    }



-- UPDATE


type Msg
    = DropdownToggle Dropdown.AllDropdowns
    | NewLanguageSelected String
    | LinkClicked Route.Route
    | OverlayLayoutClicked
    | WindowResize Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        WindowResize newWidth ->
            ( { model | screenSize = getScreenType newWidth }, Cmd.none )

        LinkClicked route ->
            let
                newDropdowns =
                    case model.dropdowns of
                        Dropdown.AllClosed ->
                            model.dropdowns

                        _ ->
                            Dropdown.AllClosed
            in
            ( { model | dropdowns = newDropdowns }, navigate model.key route )

        DropdownToggle dropdown ->
            let
                newDropdowns =
                    case model.dropdowns of
                        Dropdown.AllClosed ->
                            dropdown

                        _ ->
                            if model.dropdowns == dropdown then
                                Dropdown.AllClosed

                            else
                                dropdown
            in
            ( { model | dropdowns = newDropdowns }, Cmd.none )

        NewLanguageSelected lang ->
            ( { model
                | dropdowns = Dropdown.AllClosed
                , languages = setLangResult (Just model.languages.allData) (Just lang)
              }
            , Ports.saveDefaultLanguage lang
            )

        OverlayLayoutClicked ->
            ( { model | dropdowns = Dropdown.AllClosed }, Cmd.none )



-- PORTS


port windowResize : (Int -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions _ =
    windowResize WindowResize



-- VIEW


view :
    { page : Document msg, toMsg : Msg -> msg }
    -> Model
    -> Document msg
view { page, toMsg } model =
    let
        backgroundStyles =
            [ Background.image "/images/bg-1280x1024.jpg"
            , height fill
            , width fill
            , padding mainLayout.padding
            ]
    in
    { title = page.title
    , body =
        [ Navbar.view
            { userDropdown =
                { state =
                    case model.credentials of
                        Just _ ->
                            viewDropdownState model.dropdowns Dropdown.UserMenuOpened

                        Nothing ->
                            Dropdown.Error
                , config = userDropdownConfig model.credentials model.languages.currentData toMsg
                }
            , languageDropdown =
                { state = viewDropdownState model.dropdowns Dropdown.LanguageMenuOpened
                , config =
                    languageDropdownConfig
                        model.languages.current
                        model.languages.all
                        toMsg
                }
            }
        , row
            (List.concat
                [ backgroundStyles
                , viewOverlayLayout toMsg model.dropdowns
                ]
            )
            [ row
                [ centerX
                , height fill
                , width fill
                , htmlAttribute <| style "justify-content" "center"
                , htmlAttribute <| style "align-items" "flex-start"
                ]
                [ wrappedRow
                    [ centerX
                    , htmlAttribute <| style "align-items" "flex-start"
                    , htmlAttribute <| style "max-width" (String.fromInt mainLayout.width ++ "px")
                    , width fill
                    , height fill
                    ]
                    page.body
                ]
            ]
        ]
    }


viewOverlayLayout : (Msg -> msg) -> Dropdown.AllDropdowns -> List (Element.Attribute msg)
viewOverlayLayout toMsg current =
    case current of
        Dropdown.AllClosed ->
            []

        _ ->
            [ inFront <|
                el
                    [ Event.onClick (toMsg OverlayLayoutClicked)
                    , width fill
                    , height fill
                    ]
                    (text "")
            ]


viewDropdownState : Dropdown.AllDropdowns -> Dropdown.AllDropdowns -> Dropdown.State
viewDropdownState opened current =
    case opened of
        Dropdown.AllClosed ->
            Dropdown.Ok False

        _ ->
            Dropdown.Ok <| opened == current
