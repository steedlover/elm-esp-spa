module Spa.Page exposing
    ( Page
    , static, sandbox, element, application
    , protectedApplication
    )

{-|

@docs Page
@docs static, sandbox, element, application
@docs Upgraded, Bundle, upgrade

-}

import Components.NotFound
import Core.Utils.Route exposing (navigate)
import Shared
import Spa.Document exposing (Document)
import Spa.Generated.Route as Route
import Spa.Url exposing (Url)


type alias Page params model msg =
    { init : Shared.Model -> Url params -> ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Document msg
    , subscriptions : model -> Sub msg
    , save : model -> Shared.Model -> Shared.Model
    , load : Shared.Model -> model -> ( model, Cmd msg )
    }


static :
    { view : Url params -> Document msg
    }
    -> Page params (Url params) msg
static page =
    { init = \_ url -> ( url, Cmd.none )
    , update = \_ model -> ( model, Cmd.none )
    , view = page.view
    , subscriptions = \_ -> Sub.none
    , save = always identity
    , load = always (identity >> ignoreEffect)
    }


sandbox :
    { init : Url params -> model
    , update : msg -> model -> model
    , view : model -> Document msg
    }
    -> Page params model msg
sandbox page =
    { init = \_ url -> ( page.init url, Cmd.none )
    , update = \msg model -> ( page.update msg model, Cmd.none )
    , view = page.view
    , subscriptions = \_ -> Sub.none
    , save = always identity
    , load = always (identity >> ignoreEffect)
    }


protectedApplication :
    { init : Shared.Model -> Url params -> ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Document msg
    , save : model -> Shared.Model -> Shared.Model
    , subscriptions : model -> Sub msg
    , load : Shared.Model -> model -> ( model, Cmd msg )
    }
    -> Page params (Maybe model) msg
protectedApplication options =
    { init =
        \shared url ->
            case shared.credentials of
                Just _ ->
                    options.init shared url |> Tuple.mapFirst Just

                Nothing ->
                    ( Nothing
                    , navigate url.key Route.Login
                    )
    , update =
        \msg m ->
            case m of
                Just model ->
                    options.update msg model |> Tuple.mapFirst Just

                Nothing ->
                    ( Nothing, Cmd.none )
    , view =
        \m ->
            case m of
                Just model ->
                    options.view model

                Nothing ->
                    Components.NotFound.view
    , subscriptions =
        \m ->
            case m of
                Just model ->
                    options.subscriptions model

                Nothing ->
                    Sub.none
    , save =
        \m shared ->
            case m of
                Just model ->
                    options.save model shared

                Nothing ->
                    shared
    , load =
        \shared m ->
            case m of
                Just model ->
                    options.load shared model |> Tuple.mapFirst Just

                Nothing ->
                    ( m, Cmd.none )
    }


element :
    { init : Url params -> ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Document msg
    , subscriptions : model -> Sub msg
    }
    -> Page params model msg
element page =
    { init = \_ params -> page.init params
    , update = \msg model -> page.update msg model
    , view = page.view
    , subscriptions = page.subscriptions
    , save = always identity
    , load = always (identity >> ignoreEffect)
    }


application :
    { init : Shared.Model -> Url params -> ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Document msg
    , subscriptions : model -> Sub msg
    , save : model -> Shared.Model -> Shared.Model
    , load : Shared.Model -> model -> ( model, Cmd msg )
    }
    -> Page params model msg
application page =
    page


ignoreEffect : model -> ( model, Cmd msg )
ignoreEffect model =
    ( model, Cmd.none )
